Prerequisites 

You may need to either install the HERE Platform CLI (AKA olp cli) https://www.here.com/docs/category/command-line-interface or use HERE Data API https://www.here.com/docs/category/data-api in order to be able to create catalogs, layers, upload, and query data, …etc.

In the steps below, I am using CLI commands

1-	Create catalog.

olp catalog create byods  BYODs \
--summary "HERE platform catalog to store custom data" \
--description "HERE platform catalog to store custom data for testing purposes" \
--credentials /Users/Folder_x/Folder_y/HEREPlatformCreds.properties

2-	Create layer inside the catalog.

olp catalog layer add hrn:here:data::olp-here:byods sample_roadnetwork_linestrings Sample_RoadNetwork_LineStrings \
--interactivemap \
--summary "This layer contains sample road network line strings" \
--description "This layer contains sample road network line strings" \
--billing-tags "WaliedCheetos" \
--credentials /Users/Folder_x/Folder_y/HEREPlatformCreds.properties

3-	Upload data inside the layer.

olp catalog layer feature put hrn:here:data::olp-here:byods sample_roadnetwork_linestrings \
--data /Users/Folder_x/Folder_Z/RoadNetwork_LineStrings.json \
--credentials /Users/Folder_x/Folder_y/HEREPlatformCreds.properties


Demo

-	https://gitlab.com/waliedcheetos-public/weblab/-/tree/main/HERE_IMLs


References:

-	HERE Platform CLI https://www.here.com/docs/category/command-line-interface
-	HERE Data API https://www.here.com/docs/category/data-api
-	Get data from an interactive map layer https://www.here.com/docs/bundle/data-api-developer-guide/page/rest/getting-data-interactive.html
-	Postman collection https://gitlab.com/to_remove-public/here-sdk_iml-byod/-/tree/main/assets
