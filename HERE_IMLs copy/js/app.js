import { config } from "../../common/config/config.js";
import { loggg } from "../../common/js/logger.js";

//#region globals

var log = config.log;

var center = config.map.center;
var zoom = config.map.zoom;
var padding = config.map.padding;

var apikey = config.HERELocationPlatform.imls.dcTransit.apikey;
var catalogHrn_dcTransit = config.HERELocationPlatform.imls.dcTransit.catalogHRN;
var layerId_dcTransit = config.HERELocationPlatform.imls.dcTransit.layerID;

//#endregion


//#region IML autocomplete and search 

const inputText = document.getElementById('inputText');
const listOfMatches = document.getElementById('list-of-matches');


//using arrow function that is triggered on event 'input' inside inputText - input element in HTML
inputText.addEventListener('input', ()=> findIMLData(inputText.value));


// function to parse through IML json response and filter as per input
const findIMLData = async searchText => {
    
    // fetch returns a promise and hence needs to be used in an async function with an await call
    //.json also returns a promise and needs to be used with await
    const fetched= await fetch(`https://interactive.data.api.platform.here.com/interactive/v1/catalogs/${catalogHrn_dcTransit}/layers/${layerId_dcTransit}/search?p.SEGMENT_NA@>${searchText}&apikey=${apikey}`);
    const allIMLData = await fetched.json();
    loggg(allIMLData, log.logLevels.INFO);

    //using regular expression to find match of states with the input text
    const expression = new RegExp(`^${searchText}`, 'gi');

    //using filter - higher order array method like map - takes a function - loops through the array(here json array) and returns elements based on condition put in function
    let matches = allIMLData.features.filter(function(imlData){
        // return imlData.SEGMENT_NA.match(expression) || imlData.OBJECTID.match(expression);
        return imlData.properties.SEGMENT_NA.match(expression);
    });
    
    //makes matches as null when text in input field is cleared out
    if(searchText.length === 0){
        matches = [];
        listOfMatches.innerHTML = "";
    }

    //console.log(matches);
    displayList(matches);
     
}

// function to display output on HTML

const displayList = matches => {
    if(matches.length > 0){
        
        //map is a higher order array function. returns an array from an array
        //here returns an array of HTML as a div for each matched element

        const HTMLtext = matches.map(function(match){
            const html = `<div class="card card-body mb-4">
                <h4>
                    ${match.properties.SEGMENT_NA} (${match.properties.OBJECTID}) <span class="text-primary"> ${match.properties.type} </span>
                </h4>
                <small>
                    LAT: ${match.geometry.coordinates[0][0]} / LONG: ${match.geometry.coordinates[0][1]}
                </small>
            </div>`;

            return html;
        });
        //console.log(HTMLtext);

        //join to make a single string of HTML fromn the array of HTML.

        let HTMLmatchlist = HTMLtext.join('');
        //console.log(HTMLmatchlist);

        //putthing HTML in list-of-matches
        listOfMatches.innerHTML = HTMLmatchlist;

    }

   
}


//using array function for map!
/*
const displayList = matches => {
    if(matches.length > 0){
        const HTMLtext = matches.map(
            match=>`<div class="card card-body mb-4">
                <h4>
                    ${match.name} (${match.abbr}) <span class="text-primary"> ${match.capital} </span>
                </h4>
                <small>
                    LAT: ${match.lat} / LONG: ${match.long}
                </small>
                </div>`
        ).join('');

       // console.log(HTMLtext);

        listOfMatches.innerHTML = HTMLtext;
    }

}
*/

//#endregion

function synchronizeMaps(hereMap, leafletMap) {
    try{
    // set up view change listener on interactive map (HERE)
    hereMap.addEventListener('mapviewchange', function() {
      // on every view change take a "snapshot" of a current geo data for
      // interactive map and set this values to the second, interactive, map
      leafletMap.setView(hereMap.getCenter(), hereMap.getZoom());
    });

    // set up view change listener on interactive map (Leaflet)
    // leafletMap.on('moveend', function () {
    //     // on every view change take a "snapshot" of a current geo data for
    //     // interactive map and set this values to the second, interactive, map
    //     hereMap.getViewModel().setLookAtData({position:leafletMap.getCenter(), zoom:leafletMap.getZoom()}, true);
    // });
} catch (error) {
    loggg(error.stack, log.logLevels.ERROR);
}
}

function addIML2HEREMaps(map, imlService, catalogHrn, layerId) {
    try {


    // // HERE platform stores data in catalogs. Define Here Resource Name (HRN) of the catalog
    // const catalogHrn = catalogHrn;
    // // A catalog is a collection of layers that are managed as a single set. Define the layer that stores data
    // const layerId = layerId

    // // Instantiate the IML service
    // const service = platform.getIMLService();

    // Create a provider for the custom user defined data
    const imlProvider = new H.service.iml.Provider(imlService, catalogHrn, layerId);

    // Get the style object
    const style = imlProvider.getStyle();
    // Query the sub-section of the style configuration
    const styleConfig = style.extractConfig(['iml']);
    // Add dashes
    styleConfig.layers.iml.lines.draw.lines.dash = [1, 1];
    // Set line width per zoom level
    styleConfig.layers.iml.lines.draw.lines.width = [[5, 5000], [8, 800], [10, 200], [12, 160], [14, 60], [18, 20]];
    // Merge the style configuration back
    style.mergeConfig(styleConfig);

    // Add a tile layer to the map
    map.addLayer(new H.map.layer.TileLayer(imlProvider));
} catch (error) {
        loggg(error.stack, log.logLevels.ERROR);
    }
}

function styleVectorFeature_byType(type) {
    return type == 'Rapid Bus Transit'
      ? {
          fillColor: "yellow",
          fillOpacity: 0.7,
          color: "yellow",
          fill: true,
          weight: 1.5
        }
      : type == 'Rapid Bus'
      ? {
          fillColor: "red",
          fillOpacity: 0.7,
          color: "red",
          fill: true,
          weight: 1.5
        }
      : type == 'Street Car'
      ? {
          fillColor: "red",
          fillOpacity: 0.7,
          color: "red",
          fill: true,
          weight: 1.5
        }
      : {
          fillColor: "black",
          fillOpacity: 0.7,
          color: "black",
          fill: true,
          weight: 1.5
        };
    }


function addIML2LeafletMaps_VectorGrid(map) {
    try {
        let vectorTileLayer_IML = L.vectorGrid.protobuf(`https://interactive.data.api.platform.here.com/interactive/v1/catalogs/${catalogHrn_dcTransit}/layers/${layerId_dcTransit}/tile/web/{z}_{x}_{y}.mvtf?apikey=${apikey}&clip=true`, {
            interactive: true,
            vectorTileLayerStyles: {
                // // A function for styling features dynamically, depending on their
                // // properties and the map's zoom level
                // 'dc-transit': function(properties, zoom) {
                //     let type = properties.type;

                //     return type == 'Rapid Bus Transit'
                //     ? {
                //         fillColor: "yellow",
                //         fillOpacity: 0.7,
                //         color: "yellow",
                //         fill: true,
                //         weight: 1.5
                //       }
                //     : type == 'Rapid Bus'
                //     ? {
                //         fillColor: "red",
                //         fillOpacity: 0.7,
                //         color: "red",
                //         fill: true,
                //         weight: 1.5
                //       }
                //     : type == 'Street Car'
                //     ? {
                //         fillColor: "red",
                //         fillOpacity: 0.7,
                //         color: "red",
                //         fill: true,
                //         weight: 1.5
                //       }
                //     : {
                //         fillColor: "black",
                //         fillOpacity: 0.7,
                //         color: "black",
                //         fill: true,
                //         weight: 1.5
                //       };
                // }
            }
            // subdomains: "0123",
            // key: ***,
            // maxNativeZoom: 14
        }).addTo(map);

        vectorTileLayer_IML.on('click', function (event) {
            try {
                // L.popup()
                // .setContent(event.layer.properties)
                // .setLatLng(event.latlng)
                // .openOn(map);

                // console.log(event.layer);
                L.DomEvent.stop(event);
                alert(JSON.stringify(event.layer.properties));
                
            } catch (error) {
                loggg(error.stack, log.logLevels.ERROR);
            }
        });

    } catch (error) {
        loggg(error.stack, log.logLevels.ERROR);
    }
}
function addIML2LeafletMaps_MapboxVectorTile(map) {
    try {
        let vectorTileLayer_IML = new L.TileLayer.MVTSource({
            url: `https://interactive.data.api.platform.here.com/interactive/v1/catalogs/${catalogHrn_dcTransit}/layers/${layerId_dcTransit}/tile/web/{z}_{x}_{y}.mvtf?apikey=${apikey}&clip=true`,
            clickableLayers: ['dc-transit'],
            style: function (feature) {
                let style = {};
                let type = feature.type;

                switch (type) {
                    case 'Rapid Bus Transit':
                        style.color = 'rgba(255,0,0,0.8)';
                        style.size = 3;
                        style.selected = {
                            color: 'rgba(255,25,0,0.5)',
                            size: 4
                        };
                        break;
                    case 'Rapid Bus':
                        style.color = 'rgba(0,255,0,0.8)';
                        style.size = 3;
                        style.selected = {
                            color: 'rgba(255,25,0,0.5)',
                            size: 4
                        };
                        break;
                    case 'Street Car':
                        style.color = 'rgba(0,0,255,0.8)';
                        style.size = 3;
                        style.selected = {
                            color: 'rgba(255,25,0,0.5)',
                            size: 4
                        };
                        break;
                    default:
                        style.color = 'rgba(161,217,155,0.8)';
                        style.size = 3;
                        style.selected = {
                            color: 'rgba(255,25,0,0.5)',
                            size: 4
                        };
                        break;
                    }
                    return style;
                }
            });

        //Add layer
        map.addLayer(vectorTileLayer_IML);
        

        vectorTileLayer_IML.on('click', function (event) {
            try {
                // L.popup()
                // .setContent(event.layer.properties)
                // .setLatLng(event.latlng)
                // .openOn(map);

                // console.log(event.layer);
                L.DomEvent.stop(event);
                alert(JSON.stringify(event.layer.properties));
                
            } catch (error) {
                loggg(error.stack, log.logLevels.ERROR);
            }
        });

    } catch (error) {
        loggg(error.stack, log.logLevels.ERROR);
    }
}

function search() {
    var query = document.getElementById('searchInput').value;
    // Implement search functionality
}

document.addEventListener('DOMContentLoaded', function () {



    //#region HERE Map

    // const engineType = H.Map.EngineType['HARP'];

    // Initialize the HERE Map
    var platform = new H.service.Platform({
        apikey: apikey
    });

    // var defaultLayers = platform.createDefaultLayers({engineType});
    var defaultLayers = platform.createDefaultLayers({});

    // Instantiate the IML service
    var imlService = platform.getIMLService();

    var mapHERE = new H.Map(document.getElementById('mapContainerHERE'), defaultLayers.vector.normal.map, {
        center: center,
        zoom: zoom,
        pixelRatio: window.devicePixelRatio || 1,
        padding: padding,
        // engineType
    });

    // Enable the map interaction with the events behavior
    var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(mapHERE));

    // Create the default UI components
    var ui = H.ui.UI.createDefault(mapHERE, defaultLayers);

    // add a resize listener to make sure that the map occupies the whole container
    window.addEventListener('resize', () => mapHERE.getViewPort().resize());

    addIML2HEREMaps(mapHERE, imlService, catalogHrn_dcTransit, layerId_dcTransit);

    //#endregion

    //#region Leaflet map

    // Initialize Leaflet Map
    var mapLeaflet = L.map('mapContainerLeaflet').setView([center.lat, center.lng], zoom);
    L.tileLayer(`https://maps.hereapi.com/v3/base/mc/{z}/{x}/{y}/png8?&lang=en&style=explore.night&size=256&apiKey=${apikey}`).addTo(mapLeaflet);

    addIML2LeafletMaps_VectorGrid(mapLeaflet);
    // addIML2LeafletMaps_MapboxVectorTile(mapLeaflet);

    //#endregion

    synchronizeMaps(mapHERE, mapLeaflet);

});
