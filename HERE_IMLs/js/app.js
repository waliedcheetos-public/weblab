import { config } from "../../common/config/config.js";
import { loggg } from "../../common/js/logger.js";

//#region globals

var log = config.log;

var center = config.map.center;
var zoom = config.map.zoom;
var padding = config.map.padding;

var apikey = config.HERELocationPlatform.imls.dcTransit.apikey;
var catalogHrn_dcTransit = config.HERELocationPlatform.imls.dcTransit.catalogHRN;
var layerId_dcTransit = config.HERELocationPlatform.imls.dcTransit.layerID;

//#endregion


//#region IML autocomplete and search 


    const searchBox = document.getElementById('searchBox');
    const suggestionsList = document.getElementById('suggestions');

    // Function to fetch suggestions from API
    async function fetchSuggestions(inputValue) {
        // const response = await fetch(`https://interactive.data.api.platform.here.com/interactive/v1/catalogs/${catalogHrn_dcTransit}/layers/${layerId_dcTransit}/search?p.SEGMENT_NA=${inputValue}&apikey=${apikey}`);
        const response = await fetch(`https://interactive.data.api.platform.here.com/interactive/v1/catalogs/${catalogHrn_dcTransit}/layers/${layerId_dcTransit}/search?p.OBJECTID=${inputValue}&apikey=${apikey}`);
        const data = await response.json();
        return data.features;
    }

    // Function to display suggestions
    function showSuggestions(suggestions) {
        suggestionsList.innerHTML = '';
        suggestions.forEach(suggestion => {
            const li = document.createElement('li');
            // li.textContent = suggestion.properties.SEGMENT_NA;
            li.innerHTML = `<div class="card card-body mb-4">
            <b>
                ObjectID: ${suggestion.properties.OBJECTID}
            <b>
            </br>
            <h7>
                Segment Name: ${suggestion.properties.SEGMENT_NA} 
            </h7>
            </br>
            <h9>
                <span class="text-primary">Segment Type: ${suggestion.properties.type} </span>
            </h9>
            
        </div>`
            suggestionsList.appendChild(li);
        });
    }

    // Function to handle selection of a suggestion
    function handleSelection(selectedItem) {
        alert(`You selected: ${selectedItem.textContent}`);
    }

    // Event listener for input changes
    searchBox.addEventListener('input', async function () {
        const inputValue = this.value.trim();
        if (inputValue !== '') {
            const suggestions = await fetchSuggestions(inputValue);
            showSuggestions(suggestions);
        } else {
            suggestionsList.innerHTML = '';
        }
    });

    // Event listener for suggestion click
    suggestionsList.addEventListener('click', function (event) {
        if (event.target.tagName === 'LI') {
            handleSelection(event.target);
        }
    });

//#endregion

function synchronizeMaps(hereMap, leafletMap) {
    try{
    // set up view change listener on interactive map (HERE)
    hereMap.addEventListener('mapviewchange', function() {
      // on every view change take a "snapshot" of a current geo data for
      // interactive map and set this values to the second, interactive, map
      leafletMap.setView(hereMap.getCenter(), hereMap.getZoom());
    });

    // set up view change listener on interactive map (Leaflet)
    // leafletMap.on('moveend', function () {
    //     // on every view change take a "snapshot" of a current geo data for
    //     // interactive map and set this values to the second, interactive, map
    //     hereMap.getViewModel().setLookAtData({position:leafletMap.getCenter(), zoom:leafletMap.getZoom()}, true);
    // });
} catch (error) {
    loggg(error.stack, log.logLevels.ERROR);
}
}

function addIML2HEREMaps(map, imlService, catalogHrn, layerId) {
    try {


    // // HERE platform stores data in catalogs. Define Here Resource Name (HRN) of the catalog
    // const catalogHrn = catalogHrn;
    // // A catalog is a collection of layers that are managed as a single set. Define the layer that stores data
    // const layerId = layerId

    // // Instantiate the IML service
    // const service = platform.getIMLService();

    // Create a provider for the custom user defined data
    const imlProvider = new H.service.iml.Provider(imlService, catalogHrn, layerId);

    // Get the style object
    const style = imlProvider.getStyle();
    // Query the sub-section of the style configuration
    const styleConfig = style.extractConfig(['iml']);
    // Add dashes
    styleConfig.layers.iml.lines.draw.lines.dash = [1, 1];
    // Set line width per zoom level
    styleConfig.layers.iml.lines.draw.lines.width = [[5, 5000], [8, 800], [10, 200], [12, 160], [14, 60], [18, 20]];
    // Merge the style configuration back
    style.mergeConfig(styleConfig);

    // Add a tile layer to the map
    map.addLayer(new H.map.layer.TileLayer(imlProvider));
} catch (error) {
        loggg(error.stack, log.logLevels.ERROR);
    }
}

function styleVectorFeature_byType(type) {
    return type == 'Rapid Bus Transit'
      ? {
          fillColor: "yellow",
          fillOpacity: 0.7,
          color: "yellow",
          fill: true,
          weight: 1.5
        }
      : type == 'Rapid Bus'
      ? {
          fillColor: "red",
          fillOpacity: 0.7,
          color: "red",
          fill: true,
          weight: 1.5
        }
      : type == 'Street Car'
      ? {
          fillColor: "red",
          fillOpacity: 0.7,
          color: "red",
          fill: true,
          weight: 1.5
        }
      : {
          fillColor: "black",
          fillOpacity: 0.7,
          color: "black",
          fill: true,
          weight: 1.5
        };
    }


function addIML2LeafletMaps_VectorGrid(map) {
    try {
        let vectorTileLayer_IML = L.vectorGrid.protobuf(`https://interactive.data.api.platform.here.com/interactive/v1/catalogs/${catalogHrn_dcTransit}/layers/${layerId_dcTransit}/tile/web/{z}_{x}_{y}.mvtf?apikey=${apikey}&clip=true`, {
            interactive: true,
            vectorTileLayerStyles: {
                // // A function for styling features dynamically, depending on their
                // // properties and the map's zoom level
                // 'dc-transit': function(properties, zoom) {
                //     let type = properties.type;

                //     return type == 'Rapid Bus Transit'
                //     ? {
                //         fillColor: "yellow",
                //         fillOpacity: 0.7,
                //         color: "yellow",
                //         fill: true,
                //         weight: 1.5
                //       }
                //     : type == 'Rapid Bus'
                //     ? {
                //         fillColor: "red",
                //         fillOpacity: 0.7,
                //         color: "red",
                //         fill: true,
                //         weight: 1.5
                //       }
                //     : type == 'Street Car'
                //     ? {
                //         fillColor: "red",
                //         fillOpacity: 0.7,
                //         color: "red",
                //         fill: true,
                //         weight: 1.5
                //       }
                //     : {
                //         fillColor: "black",
                //         fillOpacity: 0.7,
                //         color: "black",
                //         fill: true,
                //         weight: 1.5
                //       };
                // }
            }
            // subdomains: "0123",
            // key: ***,
            // maxNativeZoom: 14
        }).addTo(map);

        vectorTileLayer_IML.on('click', function (event) {
            try {
                // L.popup()
                // .setContent(event.layer.properties)
                // .setLatLng(event.latlng)
                // .openOn(map);

                // console.log(event.layer);
                L.DomEvent.stop(event);
                alert(JSON.stringify(event.layer.properties));
                
            } catch (error) {
                loggg(error.stack, log.logLevels.ERROR);
            }
        });

    } catch (error) {
        loggg(error.stack, log.logLevels.ERROR);
    }
}
function addIML2LeafletMaps_MapboxVectorTile(map) {
    try {
        let vectorTileLayer_IML = new L.TileLayer.MVTSource({
            url: `https://interactive.data.api.platform.here.com/interactive/v1/catalogs/${catalogHrn_dcTransit}/layers/${layerId_dcTransit}/tile/web/{z}_{x}_{y}.mvtf?apikey=${apikey}&clip=true`,
            clickableLayers: ['dc-transit'],
            style: function (feature) {
                let style = {};
                let type = feature.type;

                switch (type) {
                    case 'Rapid Bus Transit':
                        style.color = 'rgba(255,0,0,0.8)';
                        style.size = 3;
                        style.selected = {
                            color: 'rgba(255,25,0,0.5)',
                            size: 4
                        };
                        break;
                    case 'Rapid Bus':
                        style.color = 'rgba(0,255,0,0.8)';
                        style.size = 3;
                        style.selected = {
                            color: 'rgba(255,25,0,0.5)',
                            size: 4
                        };
                        break;
                    case 'Street Car':
                        style.color = 'rgba(0,0,255,0.8)';
                        style.size = 3;
                        style.selected = {
                            color: 'rgba(255,25,0,0.5)',
                            size: 4
                        };
                        break;
                    default:
                        style.color = 'rgba(161,217,155,0.8)';
                        style.size = 3;
                        style.selected = {
                            color: 'rgba(255,25,0,0.5)',
                            size: 4
                        };
                        break;
                    }
                    return style;
                }
            });

        //Add layer
        map.addLayer(vectorTileLayer_IML);
        

        vectorTileLayer_IML.on('click', function (event) {
            try {
                // L.popup()
                // .setContent(event.layer.properties)
                // .setLatLng(event.latlng)
                // .openOn(map);

                // console.log(event.layer);
                L.DomEvent.stop(event);
                alert(JSON.stringify(event.layer.properties));
                
            } catch (error) {
                loggg(error.stack, log.logLevels.ERROR);
            }
        });

    } catch (error) {
        loggg(error.stack, log.logLevels.ERROR);
    }
}

function search() {
    var query = document.getElementById('searchInput').value;
    // Implement search functionality
}

document.addEventListener('DOMContentLoaded', function () {



    //#region HERE Map

    // const engineType = H.Map.EngineType['HARP'];

    // Initialize the HERE Map
    var platform = new H.service.Platform({
        apikey: apikey
    });

    // var defaultLayers = platform.createDefaultLayers({engineType});
    var defaultLayers = platform.createDefaultLayers({});

    // Instantiate the IML service
    var imlService = platform.getIMLService();

    var mapHERE = new H.Map(document.getElementById('mapContainerHERE'), defaultLayers.vector.normal.map, {
        center: center,
        zoom: zoom,
        pixelRatio: window.devicePixelRatio || 1,
        padding: padding,
        // engineType
    });

    // Enable the map interaction with the events behavior
    var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(mapHERE));

    // Create the default UI components
    var ui = H.ui.UI.createDefault(mapHERE, defaultLayers);

    // add a resize listener to make sure that the map occupies the whole container
    window.addEventListener('resize', () => mapHERE.getViewPort().resize());

    addIML2HEREMaps(mapHERE, imlService, catalogHrn_dcTransit, layerId_dcTransit);

    //#endregion

    //#region Leaflet map

    // Initialize Leaflet Map
    var mapLeaflet = L.map('mapContainerLeaflet').setView([center.lat, center.lng], zoom);
    L.tileLayer(`https://maps.hereapi.com/v3/base/mc/{z}/{x}/{y}/png8?&lang=en&style=explore.night&size=256&apiKey=${apikey}`).addTo(mapLeaflet);

    addIML2LeafletMaps_VectorGrid(mapLeaflet);
    // addIML2LeafletMaps_MapboxVectorTile(mapLeaflet);

    //#endregion

    synchronizeMaps(mapHERE, mapLeaflet);

});
