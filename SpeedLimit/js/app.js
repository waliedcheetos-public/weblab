import { config } from "../../common/config/config.js";
import { loggg, updateProcessingStatus } from "../../common/js/logger.js";
import { createSpeedSign, createHERELineString } from "../../common/js/helper.js";

//#region globals
let streetDataInfoBubble = null;
// Create a group to hold segments polylines
let segmentsGroup = new H.map.Group();
// Input field for coordinates
var coordinatesInput = document.getElementById('coordinatesInput');

//#endregion

document.addEventListener('DOMContentLoaded', function () {

    const engineType = H.Map.EngineType['HARP'];
    
    // Initialize the HERE Map
    var platform = new H.service.Platform({
        apikey: config.HERELocationPlatform.credentials.apikey
    });

    var defaultLayers = platform.createDefaultLayers({
        // engineType,
        pois: true,
        // opt_pois: true,
        lg:'en',
    });

    var map = new H.Map(document.getElementById('mapContainer'), defaultLayers.vector.normal.map, {
        center: config.map.center,
        zoom: config.map.zoom,
        pixelRatio: window.devicePixelRatio || 1,
        padding: config.map.padding,
        // engineType
    });

    // Enable the map interaction with the events behavior
    var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));
    
    // Create the default UI components
    var ui = H.ui.UI.createDefault(map, defaultLayers);
    
    // add a resize listener to make sure that the map occupies the whole container
    window.addEventListener('resize', () => map.getViewPort().resize());

    // Add the group to the map
    map.addObject(segmentsGroup);

    // Listen for changes in the input field
    coordinatesInput.addEventListener('keydown', function (e) {
        if(e.keyCode != 13)
            return; 
    // coordinatesInput.addEventListener('change', function () {
        var coordinates = parseCoordinates(coordinatesInput.value);
        if (coordinates) {

            clearResults();
            // Set the map's viewport to make the input location visible:
            map.getViewModel().setLookAtData({position:coordinates}, true);
        
            // //restrict zoomlevel for all subsequent requests
            // if (map.getZoom() < 13) {
            //     alert(`Current zoom level: ${map.getZoom()} - Please zoom to level 13 or more`);
            //     loggg(`Current zoom level: ${map.getZoom()} - Please zoom to level 13 or more`, config.log.logLevels.INFO);
            //     return;
            // }
    
            // let mode = (document.getElementById('gs7_rgc_mapAttributes').checked) ? 'gs7_rgc_mapAttributes' : 'gs7_rgc';

            let lat = coordinates.lat;
            let lng = coordinates.lng;
    
            (document.getElementById('gs7_rgc_mapAttributes').checked) ? getSpeedLimits_MapAttributes(lat, lng) : getSpeedLimits_GS7_RGC(lat, lng);
    

            marker.setGeometry(coordinates);
            map.setCenter(coordinates);
        } else {
            alert('Invalid coordinates format. Please enter coordinates in the format x,y or lat:x,lat:y');
            loggg('Invalid coordinates format. Please enter coordinates in the format x,y or lat:x,lat:y', config.log.logLevels.INFO);
        }
    });

    function parseCoordinates(input) {
        // Regular expression to match coordinates formats: x,y or lat:x,lat:y
        var regex = /(?:lat:)?(-?\d+(?:\.\d+)?), ?(-?\d+(?:\.\d+)?)/;
        var match = input.match(regex);
        if (match) {
            var lat = parseFloat(match[1]);
            var lng = parseFloat(match[2]);
            return { lat: lat, lng: lng };
        } else {
            return null;
        }
    }

    // Event listener for dragging the marker
    map.addEventListener('dragend', function (event) {

        loggg(`Map is centered @ ${map.getCenter().lat}, ${map.getCenter().lng}`, config.log.logLevels.DEBUG);
        clearResults();
        
        //restrict zoomlevel for all subsequent requests
        if (map.getZoom() < 13) {
            alert(`Current zoom level: ${map.getZoom()} - Please zoom to level 13 or more`);
            loggg(`Current zoom level: ${map.getZoom()} - Please zoom to level 13 or more`, config.log.logLevels.INFO);
            return;
        }

        // let mode = (document.getElementById('gs7_rgc_mapAttributes').checked) ? 'gs7_rgc_mapAttributes' : 'gs7_rgc';

        let lat = map.getCenter().lat;
        let lng = map.getCenter().lng;

        (document.getElementById('gs7_rgc_mapAttributes').checked) ? getSpeedLimits_MapAttributes(lat, lng) : getSpeedLimits_GS7_RGC(lat, lng);


        // getSpeedLimits_GS7_RGC(map.getCenter().lat, map.getCenter().lng);
        // getSpeedLimits_MapAttributes(map.getCenter().lat, map.getCenter().lng);
    });

    function clearResults() {
        try {
            if(streetDataInfoBubble)
                ui.removeBubble(streetDataInfoBubble);

            if (segmentsGroup != null)
                segmentsGroup.removeAll();

        } catch (error) {
            loggg(error.stack, config.log.logLevels.ERROR);
        }
    }

    async function getSpeedLimits_GS7_RGC(lat, lng) {
        try {
            const reversegeocoding_gs7 = await fetch(`${config.HERELocationPlatform.endpoints.reversegeocoding_gs7_streets}&at=${lat},${lng}&apikey=${config.HERELocationPlatform.credentials.apikey}`).then(response => response.json());
            
            loggg(reversegeocoding_gs7, config.log.logLevels.DEBUG);

            let position = reversegeocoding_gs7.items[0].position;
            let address = reversegeocoding_gs7.items[0].address.label;
            let speedLimit = (reversegeocoding_gs7.items[0].navigationAttributes.hasOwnProperty('speedLimits')) ? reversegeocoding_gs7.items[0].navigationAttributes.speedLimits[0].maxSpeed : '-';

            updateInfoPanel_GS7_RGC(position, address, speedLimit);

        } catch (error) {
            loggg(error.stack, config.log.logLevels.ERROR);
        }
    }

    function updateInfoPanel_GS7_RGC(position, address, speedLimit) {

        // Update the info panel
        // document.getElementById('speedLimit').innerHTML = (parseFloat(speedLimit)) ? createSpeedSign(parseFloat(speedLimit)) : createSpeedSign("(',')");
        // document.getElementById('address').textContent = address;


        let content = `<h3>Location Info</h3>
                <table>
                <tr>
                <th>Position (WeGo)</th>
                <th><a target="_blank" href="https://wego.here.com/?map=${position.lat},${position.lng},${map.getZoom()},omv" style="display:block;">${position.lat},${position.lng}</a></th>
                </tr> 
                <tr>
                <th>Position (Google)</th>
                <th><a target="_blank" href="https://www.google.com/maps/@${position.lat},${position.lng},${map.getZoom()}z" style="display:block;">${position.lat},${position.lng}</a></th>
                </tr> 
                <tr>
                <th>Streets Address</th>
                <th>${address}</th>
                </tr>  
                <tr>
                    <th>Speed Limit (KPH)</th>
                    <th>${(parseFloat(speedLimit)) ? createSpeedSign(parseFloat(speedLimit)) : createSpeedSign('-')}</th>
                </tr>
                </table>`;
                
        let infoText = `<div style=' width: 333px;'><div>${content}</div></div>`;
        streetDataInfoBubble = new H.ui.InfoBubble(position, {content: infoText});
        ui.addBubble(streetDataInfoBubble);
    }

    async function getSpeedLimits_MapAttributes(lat, lng) {
        try {
            let reversegeocoding_gs7 = await fetch(`${config.HERELocationPlatform.endpoints.reversegeocoding_gs7}&at=${lat},${lng}&apikey=${config.HERELocationPlatform.credentials.apikey}`).then(response => response.json());
            loggg(reversegeocoding_gs7.items[0].title, config.log.logLevels.INFO);

            let position = reversegeocoding_gs7.items[0].position;
            let address = reversegeocoding_gs7.items[0].address.label;
            
            // let hmcRef = reversegeocoding_gs7.items[0].hmc;
            let hmcRef = reversegeocoding_gs7.items[0].mapReferences.segments[0];
            loggg(hmcRef, config.log.logLevels.INFO);
        
        
            let mapAttributes = await fetch(`${config.HERELocationPlatform.endpoints.mapattributes_segments_withGeom_02}&segmentRefs=${((hmcRef.ref).replace('here:cm:segment:', '$1:')).replace('#', '%23')}&apikey=${config.HERELocationPlatform.credentials.apikey}`).then(response => response.json());
            loggg(mapAttributes, config.log.logLevels.INFO);

            mapAttributes.segments.forEach((segment) => {
                if (segment.hasOwnProperty('attributes')) {
                    if (segment.attributes.hasOwnProperty('ROAD_GEOM_FCN')) {
                        let  strip = createHERELineString( segment.attributes.ROAD_GEOM_FCN );
                        let segmentRefPolyline = new H.map.Polyline(strip, {
                            style: config.HERELocationPlatform.styles.polyline.lineStyle_onDefault, arrows: {width:0.8, length:1.2, frequency:3}
                            // style:{ strokeColor: 'blue', lineWidth: 13 }
                          });
                          
                        segment['polyline'] = segmentRefPolyline;

                        // // Event listeners to highlight link on mouse pointer enter/leave
                        // segmentRefPolyline.addEventListener('pointerenter', createPointerEnterLinkHandler(segment));
                        // segmentRefPolyline.addEventListener('pointerleave', createPointerLeaveLinkHandler(segment));
                        // // Event listener for link data display in InfoBubble on tap
                        // segmentRefPolyline.addEventListener('tap', createTapLinkHandler(segment));

                        segmentsGroup.addObject(segmentRefPolyline);

                        updateInfoPanel_MapAttributes(
                            position, 
                            address, 
                            segment.attributes.SPEED_LIMITS_FCN[0].FROM_REF_SPEED_LIMIT,
                            segment.attributes.SPEED_LIMITS_FCN[0].SPEED_LIMIT_UNIT,
                            segment.attributes.SPEED_LIMITS_FCN[0].TO_REF_SPEED_LIMIT,
                            segment.attributes.SPEED_LIMITS_FCN[0].SPEED_LIMIT_UNIT,
                            (segment.attributes.hasOwnProperty('TRUCK_SPEED_LIMITS_FCN')? (segment.attributes.TRUCK_SPEED_LIMITS_FCN[0].FROM_REF_SPEED_LIMIT):null),
                            (segment.attributes.hasOwnProperty('TRUCK_SPEED_LIMITS_FCN')? (segment.attributes.TRUCK_SPEED_LIMITS_FCN[0].TO_REF_SPEED_LIMIT):null)
                            );
                        }
                    }
                });

                // Set the map's viewport to make the whole link visible:
                // map.getViewModel().setLookAtData({bounds: segmentsGroup.getBoundingBox()}, true);
            } catch (error) {
            loggg(error.stack, config.log.logLevels.ERROR);
        }
    }

    function updateInfoPanel_MapAttributes(
        position, 
        address, 
        speedLimit_from_Value, speedLimit_from_Unit, 
        speedLimit_to_Value, speedLimit_to_Unit,
        speedLimit_trucks_from_Value,
        speedLimit_trucks_to_Value,
        ) {

        // Update the info panel
        // document.getElementById('speedLimit').innerHTML = (parseFloat(speedLimit)) ? createSpeedSign(parseFloat(speedLimit)) : createSpeedSign("(',')");
        // document.getElementById('address').textContent = address;


        let content = `<h3>Location Info</h3>
                <table>
                <tr>
                <th>Position (WeGo)</th>
                <th><a target="_blank" href="https://wego.here.com/?map=${position.lat},${position.lng},${map.getZoom()},omv" style="display:block;">${position.lat},${position.lng}</a></th>
                </tr> 
                <tr>
                <th>Position (Google)</th>
                <th><a target="_blank" href="https://www.google.com/maps/@${position.lat},${position.lng},${map.getZoom()}z" style="display:block;">${position.lat},${position.lng}</a></th>
                </tr> 
                <tr>
                <th>Streets Address</th>
                <th>${address}</th>
                </tr> 
                <tr>
                <td colspan="2"><b>Passenger cars SLs 🚗</b></td>
                </tr> 
                <tr>
                    <th>From Speed Limit (${speedLimit_from_Unit})</th>
                    <th>${(parseFloat(speedLimit_from_Value)) ? createSpeedSign(parseFloat(speedLimit_from_Value)) : createSpeedSign('-')}</th>
                </tr>
                <tr>
                    <th>To Speed Limit (${speedLimit_to_Unit})</th>
                    <th>${(parseFloat(speedLimit_to_Value)) ? createSpeedSign(parseFloat(speedLimit_to_Value)) : createSpeedSign('-')}</th>
                </tr>
                <tr>
                <td colspan="2"><b>Truck SLs 🚛</b></td>
                </tr> 
                <tr>
                    <th>From Speed Limit (${speedLimit_from_Unit})</th>
                    <th>${(parseFloat(speedLimit_trucks_from_Value)) ? createSpeedSign(parseFloat(speedLimit_trucks_from_Value)) : createSpeedSign('-')}</th>
                </tr>
                <tr>
                    <th>To Speed Limit (${speedLimit_to_Unit})</th>
                    <th>${(parseFloat(speedLimit_trucks_to_Value)) ? createSpeedSign(parseFloat(speedLimit_trucks_to_Value)) : createSpeedSign('-')}</th>
                </tr>
                </table>`;
                
        let infoText = `<div style=' width: 333px;'><div>${content}</div></div>`;
        streetDataInfoBubble = new H.ui.InfoBubble(position, {content: infoText});
        ui.addBubble(streetDataInfoBubble);
    }
});