import { config } from "../../common/config/config.js";
import { loggg } from "../../common/js/logger.js";

//#region globals

let log = config.log;
let center = config.map.center_Abuja_Nigeria;
let zoom = config.map.zoom;
let padding = config.map.padding;
let apikey = config.HERELocationPlatform.credentials.apikey;
let geoJSONDataSet = '../common/resources/geojson/Tenderd_Nigeria_TownNames.geojson';
let reader = null;
let mapObjectInfoBubble = null;


//#endregion

function clearResults() {
    try {
        if(mapObjectInfoBubble)
            ui.removeBubble(mapObjectInfoBubble);

    } catch (error) {
        loggg(error.stack, config.log.logLevels.ERROR);
    }
}

/**
 * Returns an instance of H.map.Icon to style the markers
 * @param {number|string} id An identifier that will be displayed as marker label
 *
 * @return {H.map.Icon}
 */
function getMarkerIcon(label) {
    // let svgMarkup =  `<svg width="30" height="30" version="1.1" xmlns="http://www.w3.org/2000/svg">
    // <g id="marker">
    //   <circle cx="15" cy="15" r="10" fill="#0099D8" stroke="#0099D8" stroke-width="4" />
    //   <text x="50%" y="50%" text-anchor="middle" fill="#FFFFFF" font-family="Arial, sans-serif" font-size="12px" dy=".3em">${label}</text>
    // </g></svg>`;

    // return new H.map.Icon(svgMarkup, {
    //     anchor: {
    //         x: 10,
    //         y: 10
    //     }
    // });

//     let svgMarkup = `<svg height="40" width="200" version="1.1" xmlns="http://www.w3.org/2000/svg">
//     <text x="5" y="30" fill="pink" stroke="blue" font-size="35">${label}</text>
//   </svg>`;

//     let svgMarkup = `<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="200" height="200">
//     <!-- Map marker -->
//     <g id="marker"> 
//         <circle cx="15" cy="15" r="10" fill="red" />
        
//         <!-- Text anchored to marker with offset -->
//         <text  font-family="Arial" font-size="13" fill="black" text-anchor="middle" dominant-baseline="middle" dy=".77em">${label}</text>
//     </g>
//   </svg>`;

  let svgMarkup = `<svg xmlns="http://www.w3.org/2000/svg" width="200" height="200">
  <!-- Map marker -->
  <circle cx="15" cy="15" r="10" fill="red" />
  
  <!-- Text anchored to marker with offset -->
  <text x="23%" y="23%" font-family="Arial" font-size="17" fill="black" >${label}</text>
</svg>`;

  return new H.map.Icon(svgMarkup, {
    anchor: {
        x: 10,
        y: 10
    }
});
}



function showGeoJSONData(dataset, map) {
    // Create GeoJSON reader which will download the specified file.
    // Shape of the file was obtained by using HERE Geocoding and Search API.
    // It is possible to customize look and feel of the objects.
    let i = 0;
    reader = new H.data.geojson.Reader(dataset, {
        // disableLegacyMode: true,
      // This function is called each time parser detects a new map object
      style: function (mapObject) {
        i++;
        // Parsed geo objects could be styled using setStyle method
        if (mapObject instanceof H.map.Polygon) {
          mapObject.setStyle({
            fillColor: 'rgba(255, 0, 0, 0.5)',
            strokeColor: 'rgba(0, 0, 255, 0.2)',
            lineWidth: 3
          });
        } 
        else if (mapObject instanceof H.map.Marker) {
            
            // mapObject.setIcon(getMarkerIcon(i));
            mapObject.setIcon(getMarkerIcon(mapObject.data.properties.Name));
            
            // add events 
            mapObject.addEventListener('tap', e => {
                clearResults();

                let content = `<h3>Location Info</h3>
                <table>
                    <tr>
                        <th>Position (WeGo)</th>
                        <th><a target="_blank" href="https://wego.here.com/?map=${mapObject.a.lat},${mapObject.a.lng},${map.getZoom()},omv" style="display:block;">${mapObject.a.lat},${mapObject.a.lng}</a></th>
                    </tr> 
                    <tr>
                        <th>Name</th>
                        <th>${mapObject.data.properties.Name}</th>
                    </tr>  
                    <tr>
                        <th>Tag</th>
                        <th>${mapObject.data.properties.Tag}</th>
                    </tr>
                    <tr>
                        <th>Type</th>
                        <th>${mapObject.data.properties.Type}</th>
                    </tr>
                </table>`;
                
            let infoText = `<div style=' width: 333px;'><div>${content}</div></div>`;
            mapObjectInfoBubble = new H.ui.InfoBubble(mapObject.a, {content: infoText});
            ui.addBubble(mapObjectInfoBubble);
        });
    }
    // map.addObject(spatialLabel);
    }
});
  
    // Start parsing the file
    reader.parse();

    // Add layer which shows GeoJSON data on the map
    // map.addLayer(reader.getLayer());
  }


  function updateMapSettingsControl() {
    const mapsettings_HERE = ui.getControl('mapsettings');
   
   try {
   
     
    let tileProvider_RasterExploreDay = new H.map.provider.ImageTileProvider({
        providesOverlays: true,
        getURL: function (column, row, zoom) {
          return `https://maps.hereapi.com/v3/base/mc/${zoom}/${column}/${row}/jpeg?lang=en&lang2=ar&style=explore.day&features=pois:all&size=512&ppi=400&apiKey=${apikey}`;
        }
     });
     let baseLayer_RasterExploreDay = new H.map.layer.TileLayer(tileProvider_RasterExploreDay,{opacity: 0.5});
     
     let tileProvider_RasterLogisticsDay_VehicleRestrictions = new H.map.provider.ImageTileProvider({
        providesOverlays: true,
        getURL: function (column, row, zoom) {
            return `https://maps.hereapi.com/v3/base/mc/${zoom}/${column}/${row}/png?lang=en&lang2=ar&style=logistics.day&features=pois:all,vehicle_restrictions:active_and_inactive&size=512&ppi=400&apiKey=${apikey}`;
        }
     });
     let baseLayer_RasterLogisticsDay_VehicleRestrictions = new H.map.layer.TileLayer(tileProvider_RasterLogisticsDay_VehicleRestrictions,{opacity: 0.5});
     
     let tileProvider_RasterFleetDay = new H.map.provider.ImageTileProvider({
        providesOverlays: true,
        getURL: function (column, row, zoom) {
            return `https://3.base.maps.api.here.com/maptile/2.1/maptile/newest/normal.day/${zoom}/${column}/${row}/512/png?style=fleet&pois&apiKey=${apikey}`;
        }
     });
     let baseLayer_RasterFleetDay = new H.map.layer.TileLayer(tileProvider_RasterFleetDay,{opacity: 0.5});
     
     let tileProvider_RasterTrafficFlow = new H.map.provider.ImageTileProvider({
        providesOverlays: false,
        getURL: function (column, row, zoom) {
            return `https://traffic.maps.hereapi.com/v3/flow/mc/${zoom}/${column}/${row}/png?apiKey=${apikey}`;
        }
     });
     let overlayLayer_RasterTrafficFlow = new H.map.layer.TileLayer(tileProvider_RasterTrafficFlow,{opacity: 1});

       //remove default mapsettings control
       ui.removeControl("mapsettings");
       // create custom one
   
       var mapSettingsControl_HERE = new H.ui.MapSettingsControl({
           baseLayers: [
             {
               label: "[LATEST] Baselayer - Raster (Explore Day)", layer: baseLayer_RasterExploreDay
           },
           {
               label: "[LATEST] Baselayer - Raster (Logistics Day - Vehicle Restrictions)", layer: baseLayer_RasterLogisticsDay_VehicleRestrictions
           },
           {
               label: "Baselayer - Vector (Day)", layer: defaultLayers.vector.normal.map
           }, 
           {
               label: "Baselayer - Raster (Day - Transit)", layer: defaultLayers.raster.normal.transit
           }
         ],
           layers :[{
               // label: "Overlay layer - Raster (Traffic Flow)", layer: overlayLayer_RasterTrafficFlow
               label: "Overlay layer - Vector (Day - Traffic)", layer: defaultLayers.vector.traffic.map
           },{
            label: 'Overlay layer - Customer GeoJSON', layer: reader.getLayer()
           }],
           //Set the control position and the map size and zoom parameters with respect to the map's viewport
           // alignment: H.ui.LayoutAlignment.TOP_RIGHT,
           alignment: H.ui.LayoutAlignment.RIGHT_BOTTOM,
       });
   
       ui.addControl("mapSettingsControl_HERE", mapSettingsControl_HERE);
   } catch (e) {
    loggg(e.stack, log.logLevels.ERROR);
   }
  
  }
  
  /**
   * Boilerplate map initialization code starts below:
   */
  // Step 1: initialize communication with the platform
  var platform = new H.service.Platform({
    apikey: apikey
  });
  var defaultLayers = platform.createDefaultLayers({pois:true});
  
  // Step 2: initialize a map
  var map = new H.Map(document.getElementById('map'), 
  defaultLayers.vector.normal.map, {
    zoom: zoom,
    center: center,
    pixelRatio: window.devicePixelRatio || 1,
    padding: padding
  });
  // add a resize listener to make sure that the map occupies the whole container
  window.addEventListener('resize', () => map.getViewPort().resize());
  
  
  // Step 3: make the map interactive
  // MapEvents enables the event system
  // Behavior implements default interactions for pan/zoom (also on mobile touch environments)
  var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));
  
  // Create the default UI components
  var ui = H.ui.UI.createDefault(map, defaultLayers);

  showGeoJSONData(geoJSONDataSet, map);
// addInfoBubble2GeoJSONData(reader.getLayer());
  updateMapSettingsControl();

  
  