
import {
  getGenerativeModel,
  fileToGenerativePart,
  updateUI,
} from "./utils/shared.js";

async function run(prompt, files) {
  const imageParts = await Promise.all(
    [...files].map(fileToGenerativePart),
  );

  const model = await getGenerativeModel({
    model: imageParts.length ? "gemini-pro-vision" : "gemini-pro",
  });

  return model.generateContentStream([...imageParts, prompt]);
}

const fileInputEl = document.querySelector("input[type=file]");
const thumbnailsEl = document.querySelector("#thumbnailContainer");

fileInputEl.addEventListener("input", () => {
  thumbnailsEl.innerHTML = "";
  for (const file of fileInputEl.files) {
    const url = URL.createObjectURL(file);
    thumbnailsEl.innerHTML += `<img class="thumb" src="${url}" onload="window.URL.revokeObjectURL(this.src)" />`;
  }
});

document
  .querySelector("#form")
  .addEventListener("submit", async (event) => {
    event.preventDefault();

    const promptEl = document.querySelector("#promptTextArea");
    const resultEl = document.querySelector("#resultPanel");

    capture(resultContainer, mapHERE, ui);

    updateUI(
      resultEl,
      () => run(promptEl.value, fileInputEl.files),
      true,
    );
  });


  //#region map logic
  import { config } from "../../common/config/config.js";
  import { loggg } from "../../common/js/logger.js";

  //#region globals

  var log = config.log;
  var center = config.map.center;
  var zoom = config.map.zoom;
  var padding = config.map.padding;

  var apikey = config.HERELocationPlatform.credentials.apikey;
  //#endregion

  // Initialize the HERE Map
  var platform = new H.service.Platform({
      apikey: apikey
  });


  let HERE_DefaultLayers = platform.createDefaultLayers({pois: true});



  let tileProvider_RasterExploreDay = new H.map.provider.ImageTileProvider({
      providesOverlays: true,
      getURL: function (column, row, zoom) {
        return `https://maps.hereapi.com/v3/base/mc/${zoom}/${column}/${row}/jpeg?lang=en&style=explore.day&features=pois:all&size=512&ppi=400&apiKey=${apikey}`;
      }
  });
  let baseLayer_RasterExploreDay = new H.map.layer.TileLayer(tileProvider_RasterExploreDay,{opacity: 0.5});

  let tileProvider_RasterLogisticsDay_VehicleRestrictions = new H.map.provider.ImageTileProvider({
      providesOverlays: true,
      getURL: function (column, row, zoom) {
          return `https://maps.hereapi.com/v3/base/mc/${zoom}/${column}/${row}/png?lang=en&lang2=ar&style=logistics.day&features=pois:all,vehicle_restrictions:active_and_inactive&size=512&ppi=400&apiKey=${apikey}`;
      }
  });
  let baseLayer_RasterLogisticsDay_VehicleRestrictions = new H.map.layer.TileLayer(tileProvider_RasterLogisticsDay_VehicleRestrictions,{opacity: 0.5});

  let tileProvider_RasterFleetDay = new H.map.provider.ImageTileProvider({
      providesOverlays: true,
      getURL: function (column, row, zoom) {
          return `https://3.base.maps.api.here.com/maptile/2.1/maptile/newest/normal.day/${zoom}/${column}/${row}/512/png?style=fleet&pois&apiKey=${apikey}`;
      }
  });
  let baseLayer_RasterFleetDay = new H.map.layer.TileLayer(tileProvider_RasterFleetDay,{opacity: 0.5});

  let tileProvider_RasterTrafficFlow = new H.map.provider.ImageTileProvider({
      providesOverlays: false,
      getURL: function (column, row, zoom) {
          return `https://traffic.maps.hereapi.com/v3/flow/mc/${zoom}/${column}/${row}/png?apiKey=${apikey}`;
      }
  });
  let overlayLayer_RasterTrafficFlow = new H.map.layer.TileLayer(tileProvider_RasterTrafficFlow,{opacity: 1});


  const engineType = H.Map.EngineType['HARP'];
  let defaultLayers = platform.createDefaultLayers({
    // engineType
  });
  

  let mapHERE = new H.Map(
    document.getElementById('mapContainer'), 
    // defaultLayers.vector.normal.map, {
    baseLayer_RasterExploreDay, {
      center: center,
      zoom: zoom,
      pixelRatio: window.devicePixelRatio || 1,
      padding: padding,
      // engineType
  });

  // Enable the map interaction with the events behavior
  let behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(mapHERE));

  // Create the default UI components
  let ui = H.ui.UI.createDefault(mapHERE, defaultLayers);

  // add a resize listener to make sure that the map occupies the whole container
  window.addEventListener('resize', () => mapHERE.getViewPort().resize());

  

const mapsettings_HERE = ui.getControl('mapsettings');
// ui.getControl('zoom').setAlignment('top-right');
ui.getControl('zoom').setAlignment('left-top');

  try {

    
      //remove default mapsettings control
      ui.removeControl("mapsettings");
      // create custom one

      var mapSettingsControl_HERE = new H.ui.MapSettingsControl({
          baseLayers: [
            {
              label: "[LATEST] Baselayer - Raster (Explore Day)", layer: baseLayer_RasterExploreDay
          },
          {
              label: "[LATEST] Baselayer - Raster (Logistics Day - Vehicle Restrictions)", layer: baseLayer_RasterLogisticsDay_VehicleRestrictions
          },
          // {
          //     label: "[LEGACY] Baselayer - Raster (Fleet Day)", layer: baseLayer_RasterFleetDay
          // }, 
          // {
          //     label: "Baselayer - Raster (Satellite)", layer: HERE_DefaultLayers.raster.satellite.map
          // }, 
          {
              label: "Baselayer - Vector (Day)", layer: HERE_DefaultLayers.vector.normal.map
          }, 
          // {
          //     label: "Baselayer - Vector (Day - Vehicle Restrictions)", layer: HERE_DefaultLayers.vector.normal.truck
          // }, 
          // {
          //     label: "Baselayer - Vector (Day - Traffic)", layer: HERE_DefaultLayers.vector.traffic.map
          // }, 
          {
              label: "Baselayer - Raster (Day - Transit)", layer: HERE_DefaultLayers.raster.normal.transit
          }
        ],
          layers :[{
              // label: "Overlay layer - Raster (Traffic Flow)", layer: overlayLayer_RasterTrafficFlow
              label: "Overlay layer - Vector (Day - Traffic)", layer: HERE_DefaultLayers.vector.traffic.map
          }],
          //Set the control position and the map size and zoom parameters with respect to the map's viewport
          // alignment: H.ui.LayoutAlignment.TOP_RIGHT,
          alignment: H.ui.LayoutAlignment.LEFT_TOP,
      });

      ui.addControl("mapSettingsControl_HERE", mapSettingsControl_HERE);

      // HERE_MapSettingsControl.setAlignment('bottom-right');
  } catch (e) {
    loggg(error.stack, log.logLevels.ERROR);
  }


  // Step 6: Create "Capture" button and place for showing the captured area
  var resultContainer = document.getElementById('panel');

  /**
 * Takes a snapshot of the map.
 *
 * @param {Element} resultContainer Reference to DOM Element to show the captured map area
 * @param {H.Map} map Reference to initialized map object
 * @param {H.ui.UI} ui Reference to UI component
 */
function capture(resultContainer, map, ui) {
  // Capturing area of the map is asynchronous, callback function receives HTML5 canvas
  // element with desired map area rendered on it.
  // We also pass an H.ui.UI reference in order to see the ScaleBar in the output.
  // If dimensions are omitted, whole veiw port will be captured
  map.capture(function(canvas) {
    if (canvas) {
      resultContainer.innerHTML = '';
      resultContainer.appendChild(canvas);

      loggg(canvas.toDataURL(), log.logLevels.DEBUG);


      // convert to Blob (async)
      canvas.toBlob( (blob) => {
        // blob.type = 'image/png';
        let file = new File( [ blob ], "mapSnapshot.png", {type: blob.type} );
        // file.type = 'image/png';
        const dT = new DataTransfer();
        dT.items.add( file );
        fileInputEl.files = dT.files;
      } );

      // thumbnailsEl.innerHTML = ''
      // thumbnailsEl.innerHTML += `<img class="thumb" src="${canvas.toDataURL()}" onload="window.URL.revokeObjectURL(this.src)" />`;


      thumbnailsEl.innerHTML = "";
      for (const file of fileInputEl.files) {
        const url = URL.createObjectURL(file);
        thumbnailsEl.innerHTML += `<img class="thumb" src="${url}" onload="window.URL.revokeObjectURL(this.src)" />`;
      }
    } else {
      // For example when map is in Panorama mode
      resultContainer.innerHTML = 'Capturing is not supported';
    }
  }, [ui],);
}

capture(resultContainer, mapHERE, ui);

function autoCatureMap(resultContainer, map, ui) {
  try{
  // set up view change listener on interactive map (HERE)
  map.addEventListener('mapviewchange', function() {
    // on every view change take a "snapshot" of a current geo data for
    // interactive map and capture the map

    capture(resultContainer, map, ui);

    
  });
} catch (error) {
  loggg(error.stack, log.logLevels.ERROR);
}
}



autoCatureMap(resultContainer, mapHERE, ui);



  //#region 