import { config } from "../../common/config/config.js";
import { loggg, updateProcessingStatus } from "../../common/js/logger.js";
import {  } from "../../common/js/helper.js";

//#region globals
let infoBubble = null;
//#endregion

// document.addEventListener('DOMContentLoaded', function () {

    const engineType = H.Map.EngineType['HARP'];
    
    // Initialize the HERE Map
    var platform = new H.service.Platform({
        apikey: config.HERELocationPlatform.credentials.apikey
    });

    var defaultLayers = platform.createDefaultLayers({engineType});

    var map = new H.Map(document.getElementById('mapContainer'), defaultLayers.vector.normal.map, {
        center: config.map.center,
        zoom: config.map.zoom,
        pixelRatio: window.devicePixelRatio || 1,
        padding: config.map.padding,
        engineType
    });

    // Enable the map interaction with the events behavior
    var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));
    
    // Create the default UI components
    var ui = H.ui.UI.createDefault(map, defaultLayers);
    
    // add a resize listener to make sure that the map occupies the whole container
    window.addEventListener('resize', () => map.getViewPort().resize());

    // Event listener for dragging the marker
    map.addEventListener('dragend', function (event) {

        loggg(`Map is centered @ ${map.getCenter().lat}, ${map.getCenter().lng}`, config.log.logLevels.DEBUG);
        clearResults();
        
        //restrict zoomlevel for all subsequent requests
        if (map.getZoom() < 13) {
            return;
        }
    });

    function clearResults() {
        try {
            if(infoBubble)
                ui.removeBubble(infoBubble);
        } catch (error) {
            loggg(error.stack, config.log.logLevels.ERROR);
        }
    }

     // Dummy data for route details
     const routeDetailsData = {
        1: [{ station: 'Station A', plannedETA: '10:00 AM', realtimeETA: '10:05 AM', predictiveETA: '10:03 AM', status: 'on time' }],
        2: [{ station: 'Station B', plannedETA: '11:00 AM', realtimeETA: '11:05 AM', predictiveETA: '11:03 AM', status: 'ahead' }],
        3: [{ station: 'Station C', plannedETA: '12:00 PM', realtimeETA: '12:05 PM', predictiveETA: '12:03 PM', status: 'over time' }],
        4: [{ station: 'Station D', plannedETA: '01:00 PM', realtimeETA: '01:05 PM', predictiveETA: '01:03 PM', status: 'on time' }],
        5: [{ station: 'Station E', plannedETA: '02:00 PM', realtimeETA: '02:05 PM', predictiveETA: '02:03 PM', status: 'ahead' }],
      };
  
    function expandCard(routeNumber) {
    // const card = document.querySelector(`.card[data-route="${routeNumber}"]`);
    const card = document.getElementById(`card_route0${routeNumber}`);
    const routeDetailsContainer = document.getElementById('route-details');
    routeDetailsContainer.innerHTML = '';

    if (card.classList.contains('expanded')) {
        card.classList.remove('expanded');
    } else {
        document.querySelectorAll('.card').forEach((c) => {
        c.classList.remove('expanded');
        });
        card.classList.add('expanded');

        // Load route details in the right panel
        const routeDetails = routeDetailsData[routeNumber];
        routeDetails.forEach((detail) => {
        const row = document.createElement('div');
        row.classList.add('station-row');
        row.innerHTML = `
            <div><strong>Station:</strong> ${detail.station}</div>
            <div><strong>Planned ETA:</strong> ${detail.plannedETA}</div>
            <div><strong>Real-time ETA:</strong> ${detail.realtimeETA}</div>
            <div><strong>Predictive ETA:</strong> ${detail.predictiveETA}</div>
            <div><strong>Status:</strong> ${detail.status}</div>
        `;
        routeDetailsContainer.appendChild(row);
        });

        // Center the map on the selected route
        const centerCoords = { lat: Math.random() * 10, lng: Math.random() * 10 }; // Replace with actual coordinates
        map.setCenter(centerCoords);
        map.setZoom(10); // Adjust the zoom level as needed
    }
    }

// });

export {expandCard}