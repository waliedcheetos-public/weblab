/*
 * Copyright (C) 2023 HERE Europe B.V. by WaliedCheetos
 * Licensed under MIT, see full license in LICENSE
 * SPDX-License-Identifier: MIT
 * License-Filename: LICENSE
 */

function getPolylineCentroid(polyline){
    var strip = polyline.getGeometry();
    var lowIndex = Math.floor((strip.getPointCount() - 1) / 2);
    var highIndex = Math.ceil(strip.getPointCount() / 2);
    var center;
    if (lowIndex === highIndex) {
        center = strip.extractPoint(lowIndex);
    } else {
        var lowPoint = strip.extractPoint(lowIndex);
        var highPoint = strip.extractPoint(highIndex);
        center = new H.geo.Point((lowPoint.lat + highPoint.lat ) / 2, (lowPoint.lng + highPoint.lng) / 2);
    }
    return center;
}

function generateRandomPoint(map, randomRange) {
    return map.getCenter().walk(Math.random() * 360, Math.random() * randomRange);
}

function createHERELineString( ROAD_GEOM_FCN ){
    // var linkGeom = segmentRef.attributes.ROAD_GEOM_FCN;
    var linkGeom = ROAD_GEOM_FCN;
    if (linkGeom) {
      var strip = new H.geo.LineString();
      for( var g=0; g < linkGeom.length; g++) {}
        var lat = linkGeom[0].LAT.split(",");
        var lon = linkGeom[0].LON.split(",");
        
        var temLat = 0, temLon = 0;
        for (let i = 0; i < lat.length; i++) {
          var latChange = parseInt(lat[i]) || 0; // to handle cases where there is no change in lat value
          var lonChange = parseInt(lon[i]) || 0; // to handle cases where there is no change in lon value
            temLat = temLat + latChange;
            temLon = temLon + lonChange;
            strip.pushPoint({
                lat: temLat / 100000,
                lng: temLon / 100000
            });
        }
      }
      return strip;
  }

function ms_2_kmh(speed) {
    if (speed == "unlimited") {
        return '&infin;';
        // return '∞';
    }
    else {
    return parseInt((speed * 3.6)/10) * 10;
    }
}

// Helper function to get color based on speed and speed limit
function getLineColor(speed, speedLimit) {                        
    
    // if (speedLimit == 'unlimited') {
    //     //meter per seceond
    //     speedLimit = 31.3889;
    // }

    //meter per seceond
    speedLimit = ((speedLimit == 'unlimited') ? 31.3889 : speedLimit);
    let opacity = 0.7;


    const speedRatio = speed / speedLimit;
    if (speedRatio > 0.75) {
        // return 'green';
        return `rgba(0, 255, 0, ${opacity})`;
    } else if (speedRatio < 0.75 && speedRatio > 0.45 ) {
        // return 'orange';
        return `rgba(255, 165, 0, ${opacity})`;
    } else {
        // return 'red';
        return `rgba(255, 0, 0, ${opacity})`;
    }
}

// Define a traffic color scheme based on the route's jam factor.
// 0 <= jamFactor < 4: No or light traffic.
// 4 <= jamFactor < 8: Moderate or slow traffic.
// 8 <= jamFactor < 10: Severe traffic.
// jamFactor = 10: No traffic, ie. the road is blocked.
// Returns null in case of no or light traffic.

function getTrafficColor(jamFactor) {
    //meter per seceond
    // speedLimit = ((speedLimit == 'unlimited') ? 31.3889 : speedLimit);

    //Note that the jamFactor indicating TrafficSpeed 
    //is calculated linear from the ratio of baseSpeedInMetersPerSecond / trafficSpeedInMetersPerSecond 
    //without taking road types and other parameters into account. 
    //Therefore, the provided jamFactor does not necessarily match exactly the traffic flow visualization on the map view (if enabled)

    //The number between 0.0 and 10.0 indicating the expected quality of travel, 
    //where 0 is high quality and 10.0 is poor quality or high level of traffic jam. 
    //-1.0 indicates that the service could not calculate Jam Factor.

    let color;
    let opacity = 1;
    if (jamFactor < 4) {
        // color = `rgba(0, 255, 0, ${opacity})`;
        color = `green`;
    } else if (jamFactor < 8) {
        // color = `rgba(255, 165, 0, ${opacity})`;
        color = `yellow`;
    } else if (jamFactor < 10) {
        // color = `rgba(255, 0, 0, ${opacity})`;
        color = `red`;
    } else {
        // color = `rgba(0, 0, 0, ${opacity})`;
        color = `black`;
    }

    return color;
}

function createSpeedSign(speedValue) {
    // generate speed icons
    var signTemplate = '<svg xmlns="http://www.w3.org/2000/svg" height="53" width="53"><circle cx="23" cy="23" r="19" stroke="red" stroke-width="7" fill="white" /><text x="23" y="23" fill="black" text-anchor="middle" alignment-baseline="central" font-weight="bold">__NO__</text></svg>';
    return signTemplate.replace(/__NO__/g, speedValue);
}

function calculateBoundingBox(coordinates) {
    let minLat = Number.POSITIVE_INFINITY;
    let minLng = Number.POSITIVE_INFINITY;
    let maxLat = Number.NEGATIVE_INFINITY;
    let maxLng = Number.NEGATIVE_INFINITY;
  
    for (const coord of coordinates) {
      minLat = Math.min(minLat, coord[0]);
      minLng = Math.min(minLng, coord[1]);
      maxLat = Math.max(maxLat, coord[0]);
      maxLng = Math.max(maxLng, coord[1]);
    }
  
    return [minLat, minLng, maxLat, maxLng];
  }

  function pad (str, max) {
    return str.length < max ? pad("0" + str, max) : str;
  }

//#region calculate interpolated jamFactor
let jam_factor_max = 10;
  
const points = [
    { ratio: 1.0, jf: 0.0 },
    { ratio: 1.25, jf: 3.0 },
    { ratio: 1.5, jf: 5.0 },
    { ratio: 2.0, jf: 7.0 },
    { ratio: 3.0, jf: 8.0 },
    { ratio: 8.0, jf: 9.0 },
    { ratio: 32.0, jf: jam_factor_max },
    { ratio: Number.MAX_VALUE, jf: jam_factor_max },
  ];
  
  function interpolateJamFactor(ratio) {
    for (let i = 0; i < points.length - 1; i++) {
      if (ratio < points[i + 1].ratio) {
        const x0 = points[i].ratio;
        const y0 = points[i].jf;
        const x1 = points[i + 1].ratio;
        const y1 = points[i + 1].jf;
  
        // Perform linear interpolation
        return y0 + (ratio - x0) * (y1 - y0) / (x1 - x0);
      }
    }
  
    // If ratio exceeds the last defined point, return the jam factor of the last point
    return points[points.length - 1].jf;
  }

//   // Example usage
//   const jamFactor = interpolateJamFactor(2.5);
//   console.log(`Jam Factor: ${jamFactor}`);

//#endregion

//#region HERE OAuth 2.0

// The generateOAuthSignature function in the attached index.js file creates the signature required for the OAuth token request. 
// It constructs a parameter string, creates a base string, and then generates a HMAC-SHA256 signature.

function generateOAuthSignature(url, method, params, consumerKey, accessSecret) {
    const parameterString = Object.keys(params).sort().map(key => `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`).join('&');
    const baseString = `${method}&${encodeURIComponent(url)}&${encodeURIComponent(parameterString)}`;
    const signingKey = `${accessSecret}&`;
    const signature = crypto.createHmac('sha256', signingKey).update(baseString).digest('base64');
    return encodeURIComponent(signature);
}

// The getOAuthToken function requests an OAuth token from HERE using your credentials and the signature generated by generateOAuthSignature.

async function getOAuthToken(oauthTokenUrl, oauthConsumerKey, accessKeySecret) {
    const params = {
        oauth_consumer_key: oauthConsumerKey,
        oauth_nonce: Date.now().toString(),
        oauth_signature_method: 'HMAC-SHA256',
        oauth_timestamp: Math.floor(Date.now() / 1000).toString(),
        oauth_version: '1.0',
        grant_type: 'client_credentials',
    };
    const encodedSignature = generateOAuthSignature(oauthTokenUrl, 'POST', params, oauthConsumerKey, accessKeySecret);

    const authHeader = `OAuth oauth_consumer_key="${params.oauth_consumer_key}",oauth_nonce="${params.oauth_nonce}",oauth_signature="${encodedSignature}",oauth_signature_method="${params.oauth_signature_method}",oauth_timestamp="${params.oauth_timestamp}",oauth_version="${params.oauth_version}"`;

    try {
        const response = await axios.post(oauthTokenUrl, 'grant_type=client_credentials', {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': authHeader
            }
        });
        return response.data.access_token;
    } catch (error) {
        console.error('Error obtaining OAuth token:', error);
        throw error;
    }
}

// // Function to query the Geocoder API
// async function queryGeocoderAPI(query, accessToken) {
//   try {
//       const response = await axios.get(geocodeUrl, {
//           params: { q: query },
//           headers: { Authorization: `Bearer ${accessToken}` }
//       });
//       const items = response.data.items;
//       if (items.length > 0) {
//           const item = items[0]; // Taking the first result for simplicity
//           console.log('Title:', item.title);
//           console.log('ID:', item.id);
//           console.log('Result Type:', item.resultType);
//           console.log('Address:', JSON.stringify(item.address, null, 2)); // Pretty-print the address object
//           console.log('Position:', JSON.stringify(item.position, null, 2)); // Pretty-print the position object
//           console.log('MapView:', JSON.stringify(item.mapView, null, 2)); // Pretty-print the mapView object
//           console.log('Scoring:', JSON.stringify(item.scoring, null, 2)); // Pretty-print the scoring object
//       } else {
//           console.log('No results found.');
//       }
//   } catch (error) {
//       console.error('Error querying Geocoder API:', error);
//   }
// }

// // Main function to orchestrate the OAuth token acquisition and Geocoder API request
// async function main() {
//   try {
//       const accessToken = await getOAuthToken();
//       console.log('Obtained Access Token:', accessToken);
//       await queryGeocoderAPI('5th Avenue New York', accessToken);
//   } catch (error) {
//       console.error('An error occurred:', error);
//   }
// }

//#endregion



export {createSpeedSign, createHERELineString, getLineColor, getTrafficColor, ms_2_kmh, getPolylineCentroid, generateRandomPoint, interpolateJamFactor, pad}
