const config = { 

    aws:{
        s3:{
            accessKeyId:'***',
            secretAccessKey:'***',
            region:'eu-west-1',
            buckets:{
                HoustonTexans:{
                    name:'nfl-houston-data',
                    region:'eu-west-1',
                }
            }
        }
    },
 
    messages:{
         process_status: {
             processing: 'PROCESSING',
             interrupted: 'INTERRUPTED',
             completed: 'COMPLETED',
         },
        NoImplemntation: 'WaliedCheetos says Hollla - Nothing implemented yet!',
        attribution: '&copy; WaliedCheetos'
    },
 
   HERELocationPlatform:{
    credentials : {
        apikey: 'JRfBr8R0vp4q7NJXV8UOxW6MD1EShYivvHNijFGe5l8',
        appid: '***',
        appcode: '***',
        oauthConsumerKey: 'your-access-key-id',
        accessKeySecret: 'your-access-key-secret',
    },
    endpoints:{
        oauthTokenUrl: 'https://account.api.here.com/oauth2/token',

        autosuggest_gs7:'https://autosuggest.search.hereapi.com/v1/autosuggest',
        autosuggest_hls:'https://places.ls.hereapi.com/places/v1/autosuggest',

        autocomplete_gs7:'https://autocomplete.search.hereapi.com/v1/autocomplete',
        autocomplete_hls:'https://autocomplete.geocoder.ls.hereapi.com/6.2/suggest.json',

        // reversegeocoding_gs7:'https://revgeocode.search.hereapi.com/v1/revgeocode?types=street&with=unnamedStreets&limit=1&show=hmcReference,streetInfo&lang=en-US',
        reversegeocoding_gs7:'https://revgeocode.search.hereapi.com/v1/revgeocode?types=street&with=unnamedStreets&limit=1&show=streetInfo&showMapReferences=segments&lang=en-US',
        // reversegeocoding_gs7_streets:'https://revgeocode.search.hereapi.com/v1/revgeocode?types=street&showNavAttributes=speedLimits,travelDirections&with=unnamedStreets&limit=1&show=hmcReference,streetInfo&lang=en-US',
        reversegeocoding_gs7_streets:'https://revgeocode.search.hereapi.com/v1/revgeocode?types=street&showNavAttributes=speedLimits,travelDirections&with=unnamedStreets&limit=1&show=streetInfo&showMapReferences=segments&lang=en-US',
        reversegeocoding_hls:'',

        geocoding_gs7:'',
        geocoding_hls:'http://geocoder.api.here.com/6.2/geocode.json',

        lookupid_gs7:'https://search.hereapi.com/v1/lookup',
        lookupid_hls:'',

        routing_v8:'https://router.hereapi.com/v8/routes',
        routing_v8_PT:'https://transit.router.hereapi.com/v8/routes',

        livetrafficflow_v7:'https://data.traffic.hereapi.com/v7/flow?locationReferencing=shape,segmentRef,tmc',
        livetrafficflow_v6:'https://traffic.ls.hereapi.com/traffic/6.2/flow.json?responseattributes=sh,fc&units=metric',
        livetrafficflow_v6_cit:'https://traffic.cit.api.here.com/traffic/6.2/flow.json?responseattributes=sh,fc&units=metric',

        mapattributes_segments:'https://smap.hereapi.com/v8/maps/attributes/segments?attributes=SPEED_LIMITS_FCn(*),LINK_ATTRIBUTE_FCn(*)',
        mapattributes_segments_withGeom:'https://smap.hereapi.com/v8/maps/attributes/segments?attributes=SPEED_LIMITS_FCn(*),LINK_ATTRIBUTE_FCn(*),ROAD_GEOM_FCn(*)',
        mapattributes_segments_withGeom_01:'https://smap.hereapi.com/v8/maps/attributes/segments?attributes=TRUCK_SPEED_LIMITS_FCn(*),SPEED_LIMITS_FCn(*),LINK_ATTRIBUTE_FCn(*),ROAD_GEOM_FCn(*)',
        mapattributes_segments_withGeom_02:'https://smap.hereapi.com/v8/maps/attributes/segments?attributes=TRUCK_SPEED_LIMITS_FCn(*),SPEED_LIMITS_FCn(*),LINK_ATTRIBUTE_FCn(*),ROAD_GEOM_FCn(*),BASIC_HEIGHT_FCn(*)',
       },
       styles:{
        polyline:{
            lineStyle_onHover : { lineWidth: 12, strokeColor: 'rgba( 10,  10, 255, 0.75)', lineJoin: 'round'},
            lineStyle_onHighlight : { lineWidth: 12, strokeColor: 'rgba(100, 100, 100, 0.50)', lineJoin: 'round'},
            lineStyle_onDefault : { lineWidth: 12, strokeColor: 'yellow', lineJoin: 'round'},
        }
    },
    imls:{
        roadnetwork_linestrings:{
            catalogHRN:'hrn:here:data::olp-here:byods',
            layerID:'sample_roadnetwork_linestrings',
            apikey:'j55YQkptTCDWVhYSHSS55sqytIhdlbTJss7qVLBQp_4',
            mvtURL:'https://interactive.data.api.platform.here.com/interactive/v1/'
        },
        dcTransit:{
            catalogHRN:'hrn:here:data::olp-here:dh-showcase-dc-transit',
            layerID:'dc-transit',
            apikey:'wuhhFoo3HHQ8Bxw68fCZe8iA_J9v4dBnRhSbkAlMup4',
            mvtURL:'https://interactive.data.api.platform.here.com/interactive/v1/catalogs/hrn:here:data::olp-here:dh-showcase-dc-transit/layers/dc-transit/tile/web/13_2343_3134.mvtf?apikey=&clip=true'
        },
        totalEnergies:{
            catalogHRN:'hrn:here:data::olp-here:total-energies',
            layerID:'geozones',
            apikey:'wuhhFoo3HHQ8Bxw68fCZe8iA_J9v4dBnRhSbkAlMup4',
            imlURL:'https://interactive.data.api.platform.here.com/interactive/v1',
            mvtURL:'https://interactive.data.api.platform.here.com/interactive/v1/catalogs/hrn:here:data::olp-here:total-energies/layers/geozones/tile/web/13_2343_3134.mvtf?apikey=&clip=true'
        }
    }

},

GoogleMapsPlatform: {
    credentials: {
        apikey: 'AIzaSyAnzeH5GOuZv5cJlRiLp1GzFJrLQTf-Nc8',
        sessiontoken:'926971b',
      },
      endpoints: {
         directions: 'https://maps.googleapis.com/maps/api/directions/json',
         routes: 'https://routes.googleapis.com/directions/v2:computeRoutes',
         tiles2d: 'https://tile.googleapis.com/v1/2dtiles',
      },
   },
 
   TomTomMapsPlatform: {
      credentials: {
         apikey: 'kGYK62ZXm0XliY1OwUYZwYpliyWGu1rP',
      },
      endpoints: {
         directions: 'https://api.tomtom.com/routing/1/calculateRoute',
      },
   },

   GCP:{
    projects:{
        waliedcheetos:{
            geminiAPI:{
                apikey_gmail:'AIzaSyAVFrTh8VqiMYK0g0Q1ooRCH0C623O43m8',
                apikey_outlook:'AIzaSyDt1C2hA5pC0ht2jVPAeuoU1Wvs8ruOxOg'
            }
        }
    }
   },
 
ArcGISLocationPlatform:{
    credentials:{
        apikey: 'AAPK478e1e0d4a3342ab9728a167558029c342jZBnlocEDgkImRQa5g10Fe9uQyshwVpCS6w9Ky2HiOV9zjWnbzcu0ekE1bDHa3',
    },
    endpoints:{

    }
},

   w3w:{
    credentials: {
        apikey: 'MMFFK8O1'
    },
 
    endpoints:{
        autosuggest : 'https://api.what3words.com/v3/autosuggest',
        reversegeocoding : 'https://api.what3words.com/v3/convert-to-3wa',
        geocoding : 'https://api.what3words.com/v3/convert-to-coordinates',
    },
    RegEx: {
        w3wFormat: /^\/{0,}[^0-9`~!@#$%^&*()+\-_=[{\]}\\|'<,.>?/";:£§º©®\s]{1,}[・.。][^0-9`~!@#$%^&*()+\-_=[{\]}\\|'<,.>?/";:£§º©®\s]{1,}[・.。][^0-9`~!@#$%^&*()+\-_=[{\]}\\|'<,.>?/";:£§º©®\s]{1,}$/i
    }
   },
 
   map :{
    logo:{
        url:'./resources/HERE_logo.wine.png',
        width:'83',
        height:''
    },
    tiles:{        
        osm:{
            url:'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
            maxZoom:'19',
            attribution:'&copy; WaliedCheetos'
        },
        here:{
            vector:{
                Tangram_BerlinStyle:{
                    url:'https://assets.vector.hereapi.com/styles/berlin/base/tangram/tilezen',
                    maxZoom:'',
                    attribution:'&copy; WaliedCheetos'
                },
                Night_Reduced:{
                    url:'https://2.base.maps.ls.hereapi.com/maptile/2.1/maptile/newest/reduced.night/{z}/{x}/{y}/512/png8'
                },
                core:{
                    url:'https://vector.hereapi.com/v2/vectortiles/base/mc/{z}/{x}/{y}/omv'
                }
            },
        }   
    },
    center:{
       lat: 25.19893,
       lng: 55.27991,
       text: 'Dubai, ARE'
    },
    center_AbuDhabi_UAE:{
        lat: 24.411958,
        lng: 54.4559497,
        text: 'Abu Dhabi, ARE'
     },
    center_WashingtonDC_USA:{
        lat: 38.90192,
        lng: -76.97605,
        text: 'Washington D.C., USA'
     },
    center_Riyadh_SAU:{
        lat: 24.68204,
        lng: 46.68725,
        text: 'Riyadh, SAU'
     },
    center_Abuja_Nigeria:{
        lat: 9.05318,
        lng: 7.47333,
        text: 'Abuja, NGA'
     },
       zoom: 3,
       tilt: 65,
       heading: 180,
       padding: {top: 50, left: 50, bottom: 50, right: 50}
   },
 
   traffic_analytics:{
    datastore:{
        url:'/data/trafficanalytics/geojson/NFLgrid_geobuf_0800_2400',
    }
   },
 
   log :{
       logLevels : {
           ERROR: 'ERROR',
           DEBUG: 'DEBUG',
           INFO: 'INFO',
           TRACE: 'TRACE',
           WARN: 'WARN'
       },
       // logLevel:'ERROR'
       logLevel:'DEBUG'
   },
 
   map_engines:{
    HERE:'HERE',
    LEAFLET:'LEAFLET'
   },
 
   corridors : [{
    corridor:[{
        id:'c0',
        name:'corridor0',
        departuretime: '',
        origin:{
            location:{lat:0, lng:0},
        },
        destination:{
            location:{lat:0, lng:0},
        },
        stations:[
            {
                station:{
                id:'s0_c0',
                name:'stations0_corridor0',
                location:{lat:0, lng:0},
                servicetime: '60',
                eta_planned:'11:30',
                eta_realtime:''
            }},
            {
                station:{
                id:'s1_c0',
                name:'stations1_corridor0',
                location:{lat:0, lng:0},
                servicetime: '60',
                eta_planned:'11:45',
                eta_realtime:''
            },},
            {
                station:{
                id:'s2_c0',
                name:'stations2_corridor0',
                location:{lat:0, lng:0},
                servicetime: '60',
                eta_planned:'12:00',
                eta_realtime:''
            },}
        ]
       },{
        id:'c1',
        name:'corridor1',
        departuretime: '',
        origin:{
            location:{lat:0, lng:0},
        },
        destination:{
            location:{lat:0, lng:0},
        },
        stations:[
            {
                station:{
                id:'s0_c1',
                name:'stations0_corridor1',
                location:{lat:0, lng:0},
                servicetime: '60',
                eta_planned:'11:30',
                eta_realtime:''
            }},
            {
                station:{
                id:'s1_c1',
                name:'stations1_corridor1',
                location:{lat:0, lng:0},
                servicetime: '60',
                eta_planned:'11:45',
                eta_realtime:''
            },},
            {
                station:{
                id:'s2_c1',
                name:'stations2_corridor1',
                location:{lat:0, lng:0},
                servicetime: '60',
                eta_planned:'12:00',
                eta_realtime:''
            },}
        ]
    }]
 }]
 }
 
 export {config}
 
 
 