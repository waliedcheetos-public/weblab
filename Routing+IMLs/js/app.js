import { config } from "../../common/config/config.js";
import { loggg } from "../../common/js/logger.js";
import { generateRandomPoint } from "../../common/js/helper.js";

//#region globals

let _log = config.log;

let _center = config.map.center_Riyadh;
let _zoom = config.map.zoom;
let _padding = config.map.padding;

let _apikey_HERELocationPlatform = config.HERELocationPlatform.credentials.apikey;
let _catalogHrn_totalEnergies = config.HERELocationPlatform.imls.totalEnergies.catalogHRN;
let _layerId_geozones = config.HERELocationPlatform.imls.totalEnergies.layerID;
let _imlURL = config.HERELocationPlatform.imls.totalEnergies.imlURL;

let _randomRange = 333;

//#endregion

function addIML2HEREMaps(map, imlService, catalogHrn, layerId) {
    try {


    // // HERE platform stores data in catalogs. Define Here Resource Name (HRN) of the catalog
    // const catalogHrn = catalogHrn;
    // // A catalog is a collection of layers that are managed as a single set. Define the layer that stores data
    // const layerId = layerId

    // // Instantiate the IML service
    // const service = platform.getIMLService();

    // Create a provider for the custom user defined data
    const imlProvider = new H.service.iml.Provider(imlService, catalogHrn, layerId);

    // Get the style object
    const style = imlProvider.getStyle();


    style.setProperty('layers.iml.polygons.ClassRestricted', {
        filter: { "properties.type": 'restricted' },
        draw: {
            polygons:{
            color: 'blue'
        },
          text: {
            priority: 0,
            text_source: 'properties.type',
            font: {
              size: '15px', fill: 'red',
              stroke: {
                color: 'white',
                width: '0.5px'
              }
            }
          }
        }
      });
    style.setProperty('layers.iml.polygons.ClassForbidden', {
        filter: { "properties.type": 'forbidden' },
        draw: {
            polygons:{
            color: 'yellow'
        },
        text: {
            priority: 0,
            text_source: 'properties.type',
            font: {
              size: '15px', fill: 'blue',
              stroke: {
                color: 'white',
                width: '0.5px'
              }
            }
          }
        }
      });

	style.setProperty('layers.iml.lines.Class0', {
        filter: { "properties.type": 'restricted' },
        draw: {
          lines: {
            order: 1,
            color: 'blue'
          },
          text: {
            priority: 0,
            text_source: 'properties.type',
            font: {
              size: '15px', fill: 'red',
              stroke: {
                color: 'white',
                width: '0.5px'
              }
            }
          }
        }
      });

	style.setProperty('layers.iml.lines.Class1', {
        filter: { "properties.type": 'forbidden' },
        draw: {
          lines: {
            order: 1,
            color: 'yellow'
          },
          text: {
            priority: 0,
            text_source: 'properties.type',
            font: {
              size: '15px', fill: 'blue',
              stroke: {
                color: 'white',
                width: '0.5px'
              }
            }
          }
        }
      });

    // // Query the sub-section of the style configuration
    // const styleConfig = style.extractConfig(['iml']);
    // // Add dashes
    // styleConfig.layers.iml.lines.draw.lines.dash = [1, 1];
    // // Set line width per zoom level
    // styleConfig.layers.iml.lines.draw.lines.width = [[5, 5000], [8, 800], [10, 200], [12, 160], [14, 60], [18, 20]];
    // // Merge the style configuration back
    // style.mergeConfig(styleConfig);

    // Add a tile layer to the map
    map.addLayer(new H.map.layer.TileLayer(imlProvider));
} catch (error) {
        loggg(error.stack, _log.logLevels.ERROR);
    }
}

function getIntersectingGeozoneswithRoute(routePolyline) {
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    const raw = JSON.stringify({
    "type": "MultiLineString",
    "coordinates": (routePolyline.toGeoJSON().geometry.coordinates)
    });

    const requestOptions = {
    method: "POST",
    headers: myHeaders,
    body: raw,
    redirect: "follow"
    };

    fetch(`${_imlURL}/catalogs/${_catalogHrn_totalEnergies}/layers/${_layerId_geozones}/spatial?apiKey=${_apikey_HERELocationPlatform}`, requestOptions)
    .then((response) => response.text())
    .then((result) => renderChart(result))
    // .then((result) => loggg(result, _log.logLevels.INFO))
    .catch((error) => console.error(error));
}

function summarizeByType(data) {
    // Initialize an object to store the summations
    const summary = {};

    // Iterate over the data array
    data.forEach(item => {
        // Extract type and weight properties from each item
        let { type, weight } = item.properties;
        // let { type, weight, backgroundColor } = item.properties;

        // If the type does not exist in the summary object, initialize it
        if (!summary[type]) {
            summary[type] = 0;
        }

        // Add the weight to the summation corresponding to the type
        summary[type] += weight;

        // backgroundColor = (item.properties.type === 'restricted' ? '#ff9800' : '#2196f3');
    });

    // Return the summarized data
    return summary;
}

// Function to render the bar chart
function renderChart(jsonData) {
    loggg(jsonData, _log.logLevels.INFO)

    // const chartData = summarizeByType(JSON.parse(jsonData).features);
    const chartData = JSON.parse(jsonData).features.map(item => ({
        type: item.properties.type,
        weight: item.properties.weight,
        name: item.properties.name,
        backgroundColor: item.properties.type === 'restricted' ? '#ff9800' : '#2196f3'
    }));

    const canvas = document.createElement('canvas');
    canvas.width = 200;
    canvas.height = 200;

    const ctx = canvas.getContext('2d');
    new Chart(ctx, {
        type: 'bar',
        data: {
            labels: chartData.map(item => item.name),
            datasets: [{
                label: chartData.map(item => item.type),
                data: chartData.map(item => item.weight),
                backgroundColor: chartData.map(item => item.backgroundColor)
            }]
        },
        options: {
            responsive: false,
            scales: {
                x: {
                    grid: {
                        display: false
                    }
                },
                y: {
                    grid: {
                        display: false
                    }
                }
            },
            plugins: {
                legend: {
                    display: false,
                    labels: {
                        type: chartData.map(item => item.type)
                    }
                }
            }
        }
    });

    // Replace existing chart
    const chartContainer = document.getElementById('chartContainer');
    chartContainer.innerHTML = '';
    chartContainer.appendChild(canvas);
}


//#region HERE Maps init
//BOILERPLATE CODE TO INITIALIZE THE MAP
const platform = new H.service.Platform({
    'apikey': _apikey_HERELocationPlatform
});

// Obtain the default map types from the platform object:
const defaultLayers = platform.createDefaultLayers();

// Instantiate the IML service
var imlService = platform.getIMLService();

// Instantiate (and display) a map:
var map = new H.Map(
    document.getElementById("mapContainerHERE"),
    defaultLayers.vector.normal.map, {
        zoom: _zoom,
        center: _center,
        pixelRatio: window.devicePixelRatio || 1,
        padding: _padding,
    });

// MapEvents enables the event system
// Behavior implements default interactions for pan/zoom (also on mobile touch environments)
const behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

// Disable zoom on double-tap to allow removing waypoints on double-tap
behavior.disable(H.mapevents.Behavior.Feature.DBL_TAP_ZOOM);

window.addEventListener('resize', () => map.getViewPort().resize());

// Create the default UI:
var ui = H.ui.UI.createDefault(map, defaultLayers, );

// Add IML layer to basemap
addIML2HEREMaps(map, imlService, _catalogHrn_totalEnergies, _layerId_geozones);

// ROUTING LOGIC STARTS HERE

// This variable holds the instance of the route polyline
let routePolyline;

/**
 * Handler for the H.service.RoutingService8#calculateRoute call
 *
 * @param {object} response The response object returned by calculateRoute method
 */
function routeResponseHandler(response) {
    const sections = response.routes[0].sections;
    const lineStrings = [];
    sections.forEach((section) => {
        // convert Flexible Polyline encoded string to geometry
        lineStrings.push(H.geo.LineString.fromFlexiblePolyline(section.polyline));
    });
    const multiLineString = new H.geo.MultiLineString(lineStrings);
    const bounds = multiLineString.getBoundingBox();

    // Create the polyline for the route
    if (routePolyline) {
        // If the routePolyline we just set has the new geometry
        routePolyline.setGeometry(multiLineString);
    } else {
        // If routePolyline is not yet defined, instantiate a new H.map.Polyline
        routePolyline = new H.map.Polyline(multiLineString, {
            style: {
                lineWidth: 5
            }
        });
    }

    // Add the polyline to the map
    map.addObject(routePolyline);

    getIntersectingGeozoneswithRoute(routePolyline, )
}

/**
 * Returns an instance of H.map.Icon to style the markers
 * @param {number|string} id An identifier that will be displayed as marker label
 *
 * @return {H.map.Icon}
 */
function getMarkerIcon(id) {
    const svgCircle = `<svg width="30" height="30" version="1.1" xmlns="http://www.w3.org/2000/svg">
  <g id="marker">
    <circle cx="15" cy="15" r="10" fill="#0099D8" stroke="#0099D8" stroke-width="4" />
    <text x="50%" y="50%" text-anchor="middle" fill="#FFFFFF" font-family="Arial, sans-serif" font-size="12px" dy=".3em">${id}</text>
  </g></svg>`;
    return new H.map.Icon(svgCircle, {
        anchor: {
            x: 10,
            y: 10
        }
    });
}

/**
 * Create an instance of H.map.Marker and add it to the map
 *
 * @param {object} position  An object with 'lat' and 'lng' properties defining the position of the marker
 * @param {string|number} id An identifier that will be displayed as marker label
 * @return {H.map.Marker} The instance of the marker that was created
 */
function addMarker(position, id) {
    const marker = new H.map.Marker(position, {
        data: {
            id
        },
        icon: getMarkerIcon(id),
        // Enable smooth dragging
        volatility: true
    });

    // Enable draggable markers
    marker.draggable = true;

    map.addObject(marker);
    return marker;
}

/**
 * This method calls the routing service to retrieve the route line geometry
 */
function updateRoute() {
    routingParams.via = new H.service.Url.MultiValueQueryParameter(
        waypoints.map(wp => `${wp.getGeometry().lat},${wp.getGeometry().lng}`));

    // Call the routing service with the defined parameters
    router.calculateRoute(routingParams, routeResponseHandler, console.error);
}

// ADD MARKERS FOR ORIGIN/DESTINATION
const origin = {lat: 24.708086, lng:46.7033}; //generateRandomPoint(map, _randomRange);
const destination = {lat: 24.687187, lng:46.655235}//generateRandomPoint(map, _randomRange);

const originMarker = addMarker(origin, 'A');
const destinationMarker = addMarker(destination, 'B');

// CALCULATE THE ROUTE BETWEEN THE TWO WAYPOINTS
// This array holds instances of H.map.Marker representing the route waypoints
const waypoints = []

// Define the routing service parameters
const routingParams = {
    'origin': `${origin.lat},${origin.lng}`,
    'destination': `${destination.lat},${destination.lng}`,
    // defines multiple waypoints
    'via': new H.service.Url.MultiValueQueryParameter(waypoints),
    'transportMode': 'car',
    'return': 'polyline'
};

// Get an instance of the H.service.RoutingService8 service
const router = platform.getRoutingService(null, 8);

// Call the routing service with the defined parameters and display the route
updateRoute();

/**
 * Listen to the dragstart and store relevant position information of the marker
 */
map.addEventListener('dragstart', function(ev) {
    const target = ev.target;
    const pointer = ev.currentPointer;
    if (target instanceof H.map.Marker) {
        // Disable the default draggability of the underlying map
        behavior.disable(H.mapevents.Behavior.Feature.PANNING);

        var targetPosition = map.geoToScreen(target.getGeometry());
        // Calculate the offset between mouse and target's position
        // when starting to drag a marker object
        target['offset'] = new H.math.Point(
            pointer.viewportX - targetPosition.x, pointer.viewportY - targetPosition.y);
    }
}, false);

/**
 * Listen to the dragend and update the route
 */
map.addEventListener('dragend', function(ev) {
    const target = ev.target;
    if (target instanceof H.map.Marker) {
        // re-enable the default draggability of the underlying map
        // when dragging has completed
        behavior.enable(H.mapevents.Behavior.Feature.PANNING);
        const coords = target.getGeometry();
        const markerId = target.getData().id;

        // Update the routing params `origin` and `destination` properties
        // in case we dragging either the origin or the destination marker
        if (markerId === 'A') {
            routingParams.origin = `${coords.lat},${coords.lng}`;
        } else if (markerId === 'B') {
            routingParams.destination = `${coords.lat},${coords.lng}`;
        }

        updateRoute();
    }
}, false);

/**
 * Listen to the drag event and move the position of the marker as necessary
 */
map.addEventListener('drag', function(ev) {
    const target = ev.target;
    const pointer = ev.currentPointer;
    if (target instanceof H.map.Marker) {
        target.setGeometry(
            map.screenToGeo(pointer.viewportX - target['offset'].x, pointer.viewportY - target['offset'].y)
        );
    }
}, false);

/**
 * Listen to the tap event to add a new waypoint
 */
map.addEventListener('tap', function(ev) {
    const target = ev.target;
    const pointer = ev.currentPointer;
    const coords = map.screenToGeo(pointer.viewportX, pointer.viewportY);

    if (!(target instanceof H.map.Marker)) {
        const marker = addMarker(coords, waypoints.length + 1);
        waypoints.push(marker);
        updateRoute();
    }
});

/**
 * Listen to the dbltap event to remove a waypoint
 */
map.addEventListener('dbltap', function(ev) {
    const target = ev.target;

    if (target instanceof H.map.Marker) {
        // Prevent origin or destination markers from being removed
        if (['origin', 'destination'].indexOf(target.getData().id) !== -1) {
            return;
        }

        const markerIdx = waypoints.indexOf(target);
        if (markerIdx !== -1) {
            // Remove the marker from the array of way points
            waypoints.splice(markerIdx, 1)
            // Iterate over the remaining waypoints and update their data
            waypoints.forEach((marker, idx) => {
                const id = idx + 1;
                // Update marker's id
                marker.setData({
                    id
                });
                // Update marker's icon to show its new id
                marker.setIcon(getMarkerIcon(id))
            });
        }

        // Remove the marker from the map
        map.removeObject(target);

        updateRoute();
    }
});

//#endregion