import { $, $$, to24HourFormat, formatRangeLabel, toDateInputFormat } from './helpers.js';
import { center, hereCredentials } from './config.js';
import { isolineMaxRange, requestIsolineShape,  } from './here.js';
import HourFilter from './HourFilter.js';
import MapRotation from './MapRotation.js';
import Search from './Search.js';

//WaliedCheetos

//#region globals

let config = {
   apikey : 'JRfBr8R0vp4q7NJXV8UOxW6MD1EShYivvHNijFGe5l8',
   }
   
     const coordinatesInput = document.getElementById('coordinatesInput');
     const errorDiv = document.getElementById('error');
   const reverseGeocodingRadio = document.getElementById('reverseGeocoding');
   const reverseGeocodingMapAttributesRadio = document.getElementById('reverseGeocodingMapAttributes');
   const checkboxContainer = document.getElementById('checkboxContainer');
   
   
   const linkGeometryCheckbox = document.getElementById('linkGeometry');
   const speedLimitCheckbox = document.getElementById('speedLimit');
   const truckSpeedLimitCheckbox = document.getElementById('truckSpeedLimit');
   const linkAttributesCheckbox = document.getElementById('linkAttributes');
   const truckAttributesCheckbox = document.getElementById('truckAttributes');
   
   //#endregion
   
   
   
   let tileProvider_RasterExploreDay = new H.map.provider.ImageTileProvider({
      providesOverlays: true,
      getURL: function (column, row, zoom) {
        return `https://maps.hereapi.com/v3/base/mc/${zoom}/${column}/${row}/jpeg?lang=en&lang2=ar&style=explore.day&features=pois:all&size=512&ppi=400&apiKey=${config.apikey}`;
      }
   });
   let baseLayer_RasterExploreDay = new H.map.layer.TileLayer(tileProvider_RasterExploreDay,{opacity: 0.5});
   
   let tileProvider_RasterLogisticsDay_VehicleRestrictions = new H.map.provider.ImageTileProvider({
      providesOverlays: true,
      getURL: function (column, row, zoom) {
          return `https://maps.hereapi.com/v3/base/mc/${zoom}/${column}/${row}/png?lang=en&lang2=ar&style=logistics.day&features=pois:all,vehicle_restrictions:active_and_inactive&size=512&ppi=400&apiKey=${config.apikey}`;
      }
   });
   let baseLayer_RasterLogisticsDay_VehicleRestrictions = new H.map.layer.TileLayer(tileProvider_RasterLogisticsDay_VehicleRestrictions,{opacity: 0.5});
   
   let tileProvider_RasterFleetDay = new H.map.provider.ImageTileProvider({
      providesOverlays: true,
      getURL: function (column, row, zoom) {
          return `https://3.base.maps.api.here.com/maptile/2.1/maptile/newest/normal.day/${zoom}/${column}/${row}/512/png?style=fleet&pois&apiKey=${config.apikey}`;
      }
   });
   let baseLayer_RasterFleetDay = new H.map.layer.TileLayer(tileProvider_RasterFleetDay,{opacity: 0.5});
   
   let tileProvider_RasterTrafficFlow = new H.map.provider.ImageTileProvider({
      providesOverlays: false,
      getURL: function (column, row, zoom) {
          return `https://traffic.maps.hereapi.com/v3/flow/mc/${zoom}/${column}/${row}/png?apiKey=${config.apikey}`;
      }
   });
   let overlayLayer_RasterTrafficFlow = new H.map.layer.TileLayer(tileProvider_RasterTrafficFlow,{opacity: 1});
   
   // Initialize HERE Map
const platform = new H.service.Platform({ apikey: hereCredentials.apikey });
const defaultLayers = platform.createDefaultLayers();
const map = new H.Map(document.getElementById('map'),       
// defaultLayers.vector.normal.map, {
   baseLayer_RasterExploreDay, {
   center,
   zoom: 12,
   pixelRatio: window.devicePixelRatio || 1
});
const behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));
const provider = map.getBaseLayer().getProvider();

//Initialize router and geocoder
const router = platform.getRoutingService();
const geocoder = platform.getGeocodingService();

window.addEventListener('resize', () => map.getViewPort().resize());

let polygon;
const marker = new H.map.Marker(center, {volatility: true});
marker.draggable = true;
map.addObject(marker);

// Add event listeners for marker movement
map.addEventListener('dragstart', evt => {
   if (evt.target instanceof H.map.Marker) behavior.disable();
}, false);
map.addEventListener('dragend', evt => {
   if (evt.target instanceof H.map.Marker) {
      behavior.enable();
      calculateIsoline(); 

      reverseGeocodingRadio.checked ? gs7_rgc(`{lat:${map.getCenter().lat},lng:${map.getCenter().lng}}`) : rgc_and_mapattributes(`{lat:${map.getCenter().lat},lng:${map.getCenter().lng}}`);
   }
}, false);
map.addEventListener('drag', evt => {
   const pointer = evt.currentPointer;
   if (evt.target instanceof H.map.Marker) {
     evt.target.setGeometry(map.screenToGeo(pointer.viewportX, pointer.viewportY));
   }
}, false);
   
   
   // Create the default UI components
   let ui = H.ui.UI.createDefault(map, defaultLayers);
   
   // add a resize listener to make sure that the map occupies the whole container
   window.addEventListener('resize', () => map.getViewPort().resize());
   
   
   
   const mapsettings_HERE = ui.getControl('mapsettings');
   // ui.getControl('zoom').setAlignment('top-right');
   // ui.getControl('zoom').setAlignment('left-top');
   
   try {
   
     
       //remove default mapsettings control
       ui.removeControl("mapsettings");
       // create custom one
   
       var mapSettingsControl_HERE = new H.ui.MapSettingsControl({
           baseLayers: [
             {
               label: "[LATEST] Baselayer - Raster (Explore Day)", layer: baseLayer_RasterExploreDay
           },
           {
               label: "[LATEST] Baselayer - Raster (Logistics Day - Vehicle Restrictions)", layer: baseLayer_RasterLogisticsDay_VehicleRestrictions
           },
           {
               label: "Baselayer - Vector (Day)", layer: defaultLayers.vector.normal.map
           }, 
           {
               label: "Baselayer - Raster (Day - Transit)", layer: defaultLayers.raster.normal.transit
           }
         ],
           layers :[{
               // label: "Overlay layer - Raster (Traffic Flow)", layer: overlayLayer_RasterTrafficFlow
               label: "Overlay layer - Vector (Day - Traffic)", layer: defaultLayers.vector.traffic.map
           }],
           //Set the control position and the map size and zoom parameters with respect to the map's viewport
           // alignment: H.ui.LayoutAlignment.TOP_RIGHT,
           alignment: H.ui.LayoutAlignment.RIGHT_BOTTOM,
       });
   
       ui.addControl("mapSettingsControl_HERE", mapSettingsControl_HERE);
   
       // HERE_MapSettingsControl.setAlignment('bottom-right');
   } catch (e) {
     console.error(error.stack);
   }
   
   //#region ep01 logic
   
   function rgc(loc) {
   
      // let gs7_rgc = `
      
      // https://revgeocode.search.hereapi.com/v1/revgeocode?
      // types=street
      // &showNavAttributes=speedLimits,travelDirections
      // &with=unnamedStreets
      // &limit=1
      // &show=streetInfo
      // &showMapReferences=segments
      // &lang=en-US
      // &at=${loc.lat},${loc.lng}
      // &apikey=${config.apikey}
      
      // `;
   
      // gs7_rgc = (gs7_rgc.replace(/\n/g, '')).trim();
   
      let gs7_rgc = `https://revgeocode.search.hereapi.com/v1/revgeocode?types=street&showNavAttributes=speedLimits,travelDirections&with=unnamedStreets&limit=1&show=streetInfo&showMapReferences=segments&lang=en-US&at=${loc.lat},${loc.lng}&apikey=${config.apikey}`;
      console.info(`${gs7_rgc}`);
   }
   
   function rgc_and_mapattributes(loc) {
      // let gs7_rgc = `
      
      // https://revgeocode.search.hereapi.com/v1/revgeocode?
      // types=street
      // &showNavAttributes=speedLimits,travelDirections
      // &with=unnamedStreets
      // &limit=1
      // &show=streetInfo
      // &showMapReferences=segments
      // &lang=en-US
      // &at=${loc.lat},${loc.lng}
      // &apikey=${config.apikey}
      
      // `;
   
      // gs7_rgc = (gs7_rgc.replace(/\n/g, '')).trim();
   
      let gs7_rgc = `https://revgeocode.search.hereapi.com/v1/revgeocode?types=street&showNavAttributes=speedLimits,travelDirections&with=unnamedStreets&limit=1&show=streetInfo&showMapReferences=segments&lang=en-US&at=${loc.lat},${loc.lng}&apikey=${config.apikey}`;
      console.info(`${gs7_rgc}`);
   
      
      // let mapattributes = `
      
      // https://smap.hereapi.com/v8/maps/attributes/segments?
      // attributes=
      // ${(linkGeometryCheckbox.checked ? 'ROAD_GEOM_FCn(*)' : '')}
      // ${(speedLimitCheckbox.checked ? ',SPEED_LIMITS_FCn(*)' : '')}
      // ${(truckSpeedLimitCheckbox.checked ? ',TRUCK_SPEED_LIMITS_FCn(*)' : '')}
      // ${(linkAttributesCheckbox.checked ? ',LINK_ATTRIBUTE_FCn(*)' : '')}
      // ${(truckAttributesCheckbox.checked ? ',TRUCK_RESTR_FCn(*)' : '')}
      // &segmentRefs=$1:327676374%230.152564
      // &apikey=${config.apikey}
      
      // `;
      
      // mapattributes = (mapattributes.replace(/\n/g, '')).trim();
   
      let mapattributes = `https://smap.hereapi.com/v8/maps/attributes/segments?attributes=${(linkGeometryCheckbox.checked ? 'ROAD_GEOM_FCn(*)' : '')}${(speedLimitCheckbox.checked ? ',SPEED_LIMITS_FCn(*)' : '')}${(truckSpeedLimitCheckbox.checked ? ',TRUCK_SPEED_LIMITS_FCn(*)' : '')}${(linkAttributesCheckbox.checked ? ',LINK_ATTRIBUTE_FCn(*)' : '')}${(truckAttributesCheckbox.checked ? ',TRUCK_RESTR_FCn(*)' : '')}&segmentRefs=$1:327676374%230.152564&apikey=${config.apikey}`;
      console.info(`${mapattributes}`);
   }
   
   document.addEventListener('DOMContentLoaded', function () {
   
   
      // let gs7_rgc = 'https://revgeocode.search.hereapi.com/v1/revgeocode?types=street&showNavAttributes=speedLimits,travelDirections&with=unnamedStreets&limit=1&show=streetInfo&showMapReferences=segments&lang=en-US&apikey=JRfBr8R0vp4q7NJXV8UOxW6MD1EShYivvHNijFGe5l8';
      // let mapattributes = `https://smap.hereapi.com/v8/maps/attributes/segments?attributes=${(linkGeometryCheckbox.checked ? 'ROAD_GEOM_FCn(*)' : '')}${(speedLimitCheckbox.checked ? ',SPEED_LIMITS_FCn(*)' : '')}${(truckSpeedLimitCheckbox.checked ? ',TRUCK_SPEED_LIMITS_FCn(*)' : '')}${(linkAttributesCheckbox.checked ? ',LINK_ATTRIBUTE_FCn(*)' : '')}${(truckAttributesCheckbox.checked ? ',TRUCK_RESTR_FCn(*)' : '')}&segmentRefs=$1:327676374%230.152564&apikey=JRfBr8R0vp4q7NJXV8UOxW6MD1EShYivvHNijFGe5l8`;
   
      // Regular expression to validate coordinates input
      const coordinatesRegex = /^(-?\d+(\.\d+)?),\s*(-?\d+(\.\d+)?)$/;
   
      coordinatesInput.addEventListener('input', function () {
          // Remove spaces from input
          const value = coordinatesInput.value.replace(/\s/g, '');
   
          // Check if input matches the required format
          if (!coordinatesRegex.test(value)) {
              errorDiv.textContent = 'Invalid coordinates format. Please enter coordinates in the format "x,y" or "lat:lng"';
          } else {
              errorDiv.textContent = '';
          }
      });
   
      reverseGeocodingRadio.addEventListener('change', function () {
          reverseGeocodingRadio.checked ? (checkboxContainer.disabled = true) : (checkboxContainer.disabled = false);
          // checkboxContainer.disabled = !reverseGeocodingRadio.checked;
   
          rgc(({lat:25,lng:55}));
      });
   
      reverseGeocodingMapAttributesRadio.addEventListener('change', function () {
          reverseGeocodingMapAttributesRadio.checked ? (checkboxContainer.disabled = false) : (checkboxContainer.disabled = true);
          // checkboxContainer.disabled = !reverseGeocodingRadio.checked;
   
          rgc_and_mapattributes(({lat:25,lng:55}));
      });
   
      speedLimitCheckbox.addEventListener('change', function () {
          // Implement logic for displaying/hiding sections based on checkbox state
      });
   });
   
   //#endregion
   
   //WaliedCheetos


//Height calculations
const height = $('#content-group-1').clientHeight || $('#content-group-1').offsetHeight;
$('.content').style.height = height + 'px';

//Manage initial state
$('#slider-val').innerText = formatRangeLabel($('#range').value, 'time');
$('#date-value').value = toDateInputFormat(new Date()); 

//Add event listeners
$$('.isoline-controls').forEach(c => c.onchange = () => calculateIsoline());
$$('.view-controls').forEach(c => c.onchange = () => calculateView());

//Tab control for sidebar
const tabs = $$('.tab');
tabs.forEach(t => t.onclick = tabify)
function tabify(evt) {
   tabs.forEach(t => t.classList.remove('tab-active'));
   if (evt.target.id === 'tab-1') {
      $('.tab-bar').style.transform = 'translateX(0)';
      evt.target.classList.add('tab-active');
      $('#content-group-1').style.transform = 'translateX(0)';
      $('#content-group-2').style.transform = 'translateX(100%)';
   } else {
      $('.tab-bar').style.transform = 'translateX(100%)';
      evt.target.classList.add('tab-active');
      $('#content-group-1').style.transform = 'translateX(-100%)';
      $('#content-group-2').style.transform = 'translateX(0)';
   }
}

//Theme control
const themeTiles = $$('.theme-tile');
themeTiles.forEach(t => t.onclick = tabifyThemes);
function tabifyThemes(evt) {
   themeTiles.forEach(t => t.classList.remove('theme-tile-active'));
   evt.target.classList.add('theme-tile-active');
   if (evt.target.id === 'day') {
      const style = new H.map.Style('https://js.api.here.com/v3/3.1/styles/omv/normal.day.yaml')
      provider.setStyle(style);
   } else { 
      const style = new H.map.Style('./resources/night.yaml');
      provider.setStyle(style);
   }
}

// // Initialize HERE Map
// const platform = new H.service.Platform({ apikey: hereCredentials.apikey });
// const defaultLayers = platform.createDefaultLayers();
// const map = new H.Map(document.getElementById('map'),       
// // defaultLayers.vector.normal.map, {
//    baseLayer_RasterExploreDay, {
//    center,
//    zoom: 12,
//    pixelRatio: window.devicePixelRatio || 1
// });
// const behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));
// const provider = map.getBaseLayer().getProvider();

// //Initialize router and geocoder
// const router = platform.getRoutingService();
// const geocoder = platform.getGeocodingService();

// window.addEventListener('resize', () => map.getViewPort().resize());

// let polygon;
// const marker = new H.map.Marker(center, {volatility: true});
// marker.draggable = true;
// map.addObject(marker);

// // Add event listeners for marker movement
// map.addEventListener('dragstart', evt => {
//    if (evt.target instanceof H.map.Marker) behavior.disable();
// }, false);
// map.addEventListener('dragend', evt => {
//    if (evt.target instanceof H.map.Marker) {
//       behavior.enable();
//       calculateIsoline(); 
//    }
// }, false);
// map.addEventListener('drag', evt => {
//    const pointer = evt.currentPointer;
//    if (evt.target instanceof H.map.Marker) {
//      evt.target.setGeometry(map.screenToGeo(pointer.viewportX, pointer.viewportY));
//    }
// }, false);

 //Initialize the HourFilter
const hourFilter = new HourFilter();

async function calculateIsoline() {
    console.log('updating...')
 
    //Configure the options object
    const options = {
       mode: $('#car').checked ? 'car' : $('#pedestrian').checked ? 'pedestrian' : 'truck',
       range: $('#range').value,
       rangeType: $('#distance').checked ? 'distance' : 'time',
       center: marker.getGeometry(),
       date: $('#date-value').value === '' ? toDateInputFormat(new Date()) : $('#date-value').value,
       time: to24HourFormat($('#hour-slider').value)
    }
 
    //Limit max ranges
    if (options.rangeType === 'distance') {
       if (options.range > isolineMaxRange.distance) {
          options.range = isolineMaxRange.distance
       }
       $('#range').max = isolineMaxRange.distance;
    } else if (options.rangeType == 'time') {
       if (options.range > isolineMaxRange.time) {
          options.range = isolineMaxRange.time
       }
       $('#range').max = isolineMaxRange.time;
    }
 
    //Format label
    $('#slider-val').innerText = formatRangeLabel(options.range, options.rangeType);
    
    //Center map to isoline
    map.setCenter(options.center, true);

//     const linestring = new H.geo.LineString();

//    const isolineShape = await requestIsolineShape(options);
//    isolineShape.forEach(p => linestring.pushLatLngAlt.apply(linestring, p));

//    if (polygon !== undefined) {
//       map.removeObject(polygon);
//    }

//    polygon = new H.map.Polygon(linestring, {
//       style: {
//          fillColor: 'rgba(74, 134, 255, 0.3)',
//          strokeColor: '#4A86FF',
//          lineWidth: 2
//       }
//    });
//    map.addObject(polygon);

//    //Enable bar graph for car and time options
//    if (options.mode === 'car' && options.rangeType === 'time') {
//     const promises = [];
//     for (let i = 0; i < 24; i++) {
//        options.time = to24HourFormat(i);
//        promises.push(requestIsolineShape(options))
//     }
//     const polygons = await Promise.all(promises);
//     const areas = polygons.map(x => turf.area(turf.polygon([x])));
//     hourFilter.setData(areas);
//  } else {
//     hourFilter.hideData();
//  }

 }

 const rotation = new MapRotation(map);
function calculateView() {
   const options = {
      theme: $('#day').checked ? 'day' : 'night',
      static: $('#static').checked 
   }
   if (options.static) {
      rotation.stop();
   } else {
      rotation.start();
   }
}

// new Search('Berlin, DEU');
new Search(center.text);






export { calculateIsoline, marker, router, geocoder }